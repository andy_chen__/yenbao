﻿var app = angular.module('App', []);
app.controller('messageController', function ($scope, $http) {
    var BIGERQAnum = 10;
    $http.post("./frontendapi/LOGIN", { id : window.localStorage["userid"] , pwd : window.localStorage["userpwd"] }
    ).then(function mySucces(response) {
        $scope.LOGINdata = response.data;
        if ($scope.LOGINdata == 'nologin') {
        } 
        else {
            $http.post("./frontendapi/BIGERmessageQA", { m_id: response.data[0].id ,num: BIGERQAnum}
            ).then(function youSucces(response) {
                for (var i in response.data.QAmessage) {
                    response.data.QAmessage[i].QAreplytime = new Date(response.data.QAmessage[i].QAreplytime).toLocaleDateString();
                    response.data.QAmessage[i].QAreplytime = (response.data.QAmessage[i].QAreplytime).replace("-", "/");
                    response.data.QAmessage[i].QAreplytime = (response.data.QAmessage[i].QAreplytime).replace("-", "/");
                }
                $scope.memberQAmessage = response.data.QAmessage;
            })
        }
    });

    $scope.memberQAclick = function (id) {
        $http.post("./frontendapi/QAclick", { id : id }
        ).then(function mySucces(response) {
        });
    };
    $scope.BIGERQAmessageloadmore = function () {
        BIGERQAnum += 10;
        $http.post("./frontendapi/LOGIN", { id : window.localStorage["userid"] , pwd : window.localStorage["userpwd"] }
        ).then(function mySucces(response) {
            $scope.LOGINdata = response.data;
            if ($scope.LOGINdata == 'nologin') {
            } 
            else {
                $http.post("./frontendapi/BIGERmessageQA", { m_id: response.data[0].id , num: BIGERQAnum }
                ).then(function youSucces(response) {
                    for (var i in response.data.QAmessage) {
                        response.data.QAmessage[i].QAreplytime = new Date(response.data.QAmessage[i].QAreplytime).toLocaleDateString();
                        response.data.QAmessage[i].QAreplytime = (response.data.QAmessage[i].QAreplytime).replace("-", "/");
                        response.data.QAmessage[i].QAreplytime = (response.data.QAmessage[i].QAreplytime).replace("-", "/");
                    }
                    $scope.memberQAmessage = response.data.QAmessage;
                })
            }
        });
    };
});

﻿var app = angular.module('App', []);
app.controller('ctrl', function ($scope, $http) {
    var code;
    $scope.createCode = function () {
        code = "";
        var codeLength = 6; //验证码的长度
        var checkCode = document.getElementById("checkCode");
        var codeChars = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'); //所有候选组成验证码的字符，当然也可以用中文的
        for (var i = 0; i < codeLength; i++) {
            var charNum = Math.floor(Math.random() * 52);
            code += codeChars[charNum];
        }
        if (checkCode) {
            checkCode.className = "code";
            checkCode.innerHTML = code;
        }
    }
    $scope.validateCode = function () {
        var inputCode = document.getElementById("inputCode").value;
        if (inputCode.length <= 0) {
            alert("請輸入驗證碼！");
            return 1;
        }
        else if (inputCode.toUpperCase() != code.toUpperCase()) {
            alert("驗證碼輸入錯誤！");
            $scope.createCode();
            document.getElementById("inputCode").value = "";
            return 2;
        }
        else {
            return 0;
        }
    }
    $scope.deletedata = function () {
        $('#m_account').val("");
        $('#m_pwd1').val("");
        $('#m_pwd2').val("");
        $('#m_name').val("");
        $('#m_phone').val("");
        $('#m_birthday').val("");
        $('#m_email').val("");
        $('#m_zipcode').val("");
        $('#m_country').val("");
        $('#m_city').val("");
        $('#m_township').val("");
        $('#m_address').val("");
        $('#inputCode').val("");
    }
    $scope.submit = function () {
        var n = $scope.validateCode();
        if (n == 0) {
            var birthday = ($('#m_birthday').val()).replace("/", "-")
            var emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
            var dateRule = /^((19|20)?[0-9]{2}[- -.](0?[1-9]|1[012])[- -.](0?[1-9]|[12][0-9]|3[01]))*$/;
            var APRule = /^(?=.*\d).{6,20}|(?=.*[a-z]).{6,20}|(?=.*[A-Z]).{6,20}$/;
            if ($('#m_email').val().search(emailRule) == -1) {
                alert("請確認電子郵件是否正確");
            }
            else if (birthday.search(dateRule) == -1) {
                alert("生日格式錯誤(yyyy/mm/dd)");
            }
            else if ($("#m_zipcode").val() == '' || $("#m_country").val() == '' || $("#m_city").val() == '' || $("#m_township").val() == '' || $("#m_address").val() == '') {
                alert('地址不完整');
            }
            else if ($("#m_name").val() == '') {
                alert('姓名未填寫');
            }
            else if ($("#m_phone").val() == '') {
                alert('電話未填寫');
            }
            else if ($("#m_birthday").val() == '') {
                alert('生日未填寫');
            }
            else if ($('#m_email').val() == '') {
                alert('電子郵件未填寫');
            }
            else if ($("#m_account").val().search(APRule) == -1) {
                alert('帳號格式不正確');
            }
            else if ($("#m_pwd1").val().search(APRule) == -1) {
                alert('密碼格式不正確');
            }
            else if ($("#m_pwd1").val() != $("#m_pwd2").val()) {
                alert('密碼不正確');
            }
            else {
                $.post("./frontendapi/memberonlineinsert",
                    {
                        'm_name': $('#m_name').val(),
                        'm_phone': $('#m_phone').val(),
                        'm_gender': $('input:radio:checked[name="genderRadio"]').val(),
                        'm_birthday': $('#m_birthday').val(),
                        'm_email': $('#m_email').val(),
                        'm_city': $('#m_country').val() + $('#m_city').val(),
                        'm_township': $('#m_township').val(),
                        'm_postalcode': $('#m_zipcode').val(),
                        'm_address': $('#m_address').val(),
                        'm_account': $('#m_account').val(),
                        'm_password': $('#m_pwd1').val(),
                    }
                ).done(function (result) {
                    if (result == 'IDrepeat') {
                        alert('此帳號已被使用');
                    }
                    else {
                        alert('註冊成功，歡迎加入眼寶!');
                        location.href = './';
                    }
                });


            }
        }
    }

    $scope.createCode();
});

($(function () {
    /*address*/
    $('#twzipcode').twzipcode({
        // 依序套用至縣市、鄉鎮市區及郵遞區號框
        'css': ['county', 'district', 'zipcode']
    });
    $('#accmountP').hide();
    $('#pwd1P').hide();
    $('#pwd2P').hide();
}));
function accountTooltip(s_id) {
    var APRule = /^(?=.*\d).{6,20}|(?=.*[a-z]).{6,20}|(?=.*[A-Z]).{6,20}$/;
    if ($('#' + s_id).val().search(APRule) == -1) {
        $('#accmountP').show();
    }
    else {
        $('#accmountP').hide();
    }
};
function pwd1Tooltip(s_id) {
    var APRule = /^(?=.*\d).{6,20}|(?=.*[a-z]).{6,20}|(?=.*[A-Z]).{6,20}$/;
    if ($('#' + s_id).val().search(APRule) == -1) {
        $('#pwd1P').show();
    }
    else {
        $('#pwd1P').hide();
    }
};
function pwd2Tooltip(s_id) {
    var APRule = /^(?=.*\d).{6,20}|(?=.*[a-z]).{6,20}|(?=.*[A-Z]).{6,20}$/;
    if ($('#' + s_id).val().search(APRule) == -1) {
        $('#pwd2P').show();
    }
    else {
        $('#pwd2P').hide();
    }
};
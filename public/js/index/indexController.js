﻿//var app = angular.module('App', []);
app.factory("shareScope",function(){
	return {
		mainPage:"./main"
	}
})

app.factory("mobileMenuScope", function () {
    return {
        showStatue:false
    }
})

/*通知*/
app.controller('noticeNavController', function ($scope,$http, shareScope) {
    $scope.pageManager = function (page) {
        shareScope.mainPage = page;
    }
    $http.post("./frontendapi/LOGIN", { id : window.localStorage["userid"] , pwd : window.localStorage["userpwd"] }
    ).then(function mySucces(response) {
        $scope.LOGINdata = response.data;
        if ($scope.LOGINdata == 'nologin') {
        } 
        else {
            $http.post("./frontendapi/messageQA", { m_id: response.data[0].id }
            ).then(function youSucces(response) {
                for (var i in response.data.QAmessage) {
                    response.data.QAmessage[i].QAreplytime = new Date(response.data.QAmessage[i].QAreplytime).toLocaleDateString();
                    response.data.QAmessage[i].QAreplytime = (response.data.QAmessage[i].QAreplytime).replace("-", "/");
                    response.data.QAmessage[i].QAreplytime = (response.data.QAmessage[i].QAreplytime).replace("-", "/");
                }
                $scope.memberQAmessage = response.data.QAmessage;
                $scope.memberQAnum = response.data.tic[0].NUM;
                if ($scope.memberQAnum == 0) {
                    $('#memberQAnum').hide();
                }
                else {
                    $('#memberQAnum').show();
                    $('#memberQAnum').html($scope.memberQAnum);
                }
            })
        }
    });
    $scope.memberQAclick = function (id) {
        $http.post("./frontendapi/QAclick", { id : id }
        ).then(function mySucces(response) {
        });
    };
})

/*手機選單*/
app.controller('mobileMenuController', function ($scope, shareScope, mobileMenuScope,$http) {
    mobileMenuScope.showStatue = false;
	$scope.pageManager=function(page){
	    shareScope.mainPage = page;
	    if (mobileMenuScope.showStatue) {
	        mobileMenuScope.showStatue = false;
	    }
	}
	$scope.$watch(function () { return mobileMenuScope.showStatue; }, function (newValue, oldValue) {
	    if (newValue !== oldValue) $scope.mobileMenuShow = mobileMenuScope.showStatue;
    });
    $scope.memberhome = function () {
        $http.post("./frontendapi/LOGIN", { id : window.localStorage["userid"] , pwd : window.localStorage["userpwd"] }
        ).then(function mySucces(response) {
            $scope.LOGINdata = response.data;
            if ($scope.LOGINdata == 'nologin') {
                $('#login-modal').modal();
                window.localStorage["memberid"] = '';
            } 
            else {
                location.href = './memberArea';
                window.localStorage["memberid"] = $scope.LOGINdata[0].id;
            }
        });
    }
})

/*網頁選單*/
app.controller('webMenuController', function ($scope, shareScope, mobileMenuScope,$http) {
    $scope.pageManager = function (page) {
        shareScope.mainPage = page;
        if (mobileMenuScope.showStatue) {
            mobileMenuScope.showStatue = false;
        }
	}
	
	$scope.mobileMenuShow = function () {
	    if (mobileMenuScope.showStatue) {
	        mobileMenuScope.showStatue = false;
	    } else {
	        mobileMenuScope.showStatue = true;
	    }
    }
    $scope.memberhome = function () {
        $http.post("./frontendapi/LOGIN", { id : window.localStorage["userid"] , pwd : window.localStorage["userpwd"] }
        ).then(function mySucces(response) {
            $scope.LOGINdata = response.data;
            if ($scope.LOGINdata == 'nologin') {
                $('#login-modal').modal();
                window.localStorage["memberid"] = '';
            } 
            else {
                location.href = './memberArea';
                window.localStorage["memberid"] = $scope.LOGINdata[0].id;
            }
        });
    }
})

/*登入*/
app.controller('loginController', function ($scope, $http){
    //載入時自動登入
    $http.post("./frontendapi/LOGIN", { id : window.localStorage["userid"] , pwd : window.localStorage["userpwd"] }
    ).then(function mySucces(response) {
        $scope.LOGINdata = response.data;
        window.localStorage["LOGINdata"] = $scope.LOGINdata;
        if ($scope.LOGINdata == 'nologin') {
            login(0);
            window.localStorage["memberid"] = '';
        } 
        else {
            login(1);
            $('#hello').html($scope.LOGINdata[0].name + '，您好');
            window.localStorage["memberid"] = $scope.LOGINdata[0].id;
        }
    });
    //登入
    if (window.localStorage["rememberID"] == '') { }
    else { var checkedpwd = $('#rememberpwd').prop("checked",1); }
    $scope.userlogin = function () {
        var goback = ($('#userid').val()).substr(0, 1);
        if (goback == '#') {
            $http.post("./frontendapi/backendlogin", { id : $('#userid').val() , pwd : $('#userpwd').val() }
            ).then(function mySucces(response) {
                $scope.B_LOGINdata = response.data;
                if ($scope.B_LOGINdata == 'nologin') {
                    login(0);
                    window.localStorage["memberid"] = '';
                    alert("登入失敗，帳號或密碼不正確，或是其中有不符合a~z,A~Z,0~9之字元。");
                } 
                else {
                    login(0);
                    var url = "./backend";
                    var newWindow = window.open(url, "Lobby");
                    if (!newWindow) return false;
                    var html = "";
                    html += "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";
                    html += "<input type='text' name='id' value='" + $scope.B_LOGINdata.id + "'/>";
                    html += "<input type='text' name='pwd' value='" + $scope.B_LOGINdata.pwd + "'/>";
                    html += "</form><script type='text/javascript'>document.getElementById(\"formid\").submit()</script></body></html>";
                    newWindow.document.write(html);
                    return newWindow;
                }
            });
            
        }
        else { 
            $http.post("./frontendapi/LOGIN", { id : $('#userid').val() , pwd : $('#userpwd').val() }
            ).then(function mySucces(response) {
                $scope.LOGINdata = response.data;
                if ($scope.LOGINdata == 'nologin') {
                    login(0);
                    window.localStorage["memberid"] = '';
                    alert("登入失敗，帳號或密碼不正確");
                } 
                else {
                    login(1);
                    $('#hello').html($scope.LOGINdata[0].name + '，您好');
                    window.localStorage["userid"] = $('#userid').val();
                    window.localStorage["userpwd"] = $('#userpwd').val();
                    window.localStorage["memberid"] = $scope.LOGINdata[0].id;
                    var checkedpwd = $('#rememberpwd').prop("checked");
                    if (checkedpwd == true) {
                        window.localStorage["rememberID"] = $('#userid').val();
                        window.localStorage["rememberPWD"] = $('#userpwd').val();
                    } else {
                        window.localStorage["rememberID"] = '';
                        window.localStorage["rememberPWD"] = '';
                    }
                    window.location.reload();
                }
            });
        }
    }

})

/*主頁*/
app.controller('pageController',function($scope,shareScope){
	$scope.page=shareScope.mainPage;
	$scope.$watch(function () { return shareScope.mainPage; }, function (newValue, oldValue) {
        if (newValue !== oldValue) $scope.page=shareScope.mainPage;
    });
})
 $('#userid').val(window.localStorage["rememberID"]) ;
$('#userpwd').val(window.localStorage["rememberPWD"]);
function login(x) {
    if (x == 1) {
        document.getElementById("logout-btn").style.display = 'block';
        document.getElementById("message").style.display = 'block';
        document.getElementById("hello").style.display = 'block';
        document.getElementById("login-btn").style.display = 'none';
    }
    else if (x == 0) {
        document.getElementById("logout-btn").style.display = 'none';
        document.getElementById("message").style.display = 'none';
        document.getElementById("hello").style.display = 'none';
        $('#hello').html('');
        document.getElementById("login-btn").style.display = 'block';
        $('#userid').val(window.localStorage["rememberID"]);
        $('#userpwd').val(window.localStorage["rememberPWD"]);
        window.localStorage["LOGINdata"] = '';
        window.localStorage["userid"] = '';
        window.localStorage["userpwd"] = '';
    }
}
function forgetpsd() {
    location.href = './contactUs';
};
function gohomepag() {
    location.href = './';
};
/*網頁版選單特效*/
$(function(){
    $(".navbar-nav li").hover(
        function () {
            $(this).addClass("Hover");
        },
        function () {
            $(this).removeClass("Hover");
        }
    )
})
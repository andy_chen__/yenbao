﻿var app = angular.module('App', []);
app.controller('carctrl', function ($scope, $http) {
    $http.post("../../frontendapi/CARload", { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] }
    ).then(function mySucces(response) {
        if (response.data.information[0]) {
            $scope.cardata = response.data.information;
            $scope.totalprice = 0;
            for (i in $scope.cardata) { 
                $scope.totalprice += $scope.cardata[i].totalprice;
            }
        }
        else {
            location.href = './onlineMall';
        }
    });
    var amount;//數量佔存
    //打開修改數量
    $scope.openupdateamount = function (ind) {
        $scope.updateamountdata = $scope.cardata[ind];
        amount = $scope.updateamountdata.amount;
        $('#amountinput').val(amount);
    };
    //加一
    $scope.quantityNumberD = function () {
        if (amount == 1 || amount == 0) { }
        else { amount -= 1; $('#amountinput').val(amount); }
    };
    //減一
    $scope.quantityNumberU = function () {
        if (amount == $scope.updateamountdata.p_amount || amount == 0) { }
        else { amount += 1; $('#amountinput').val(amount); }
    };
    //修改數量
    $scope.updatecaramount = function () {
        $http.post("../../frontendapi/CARupdateamount", { car_id : $scope.updateamountdata.carid , amount : amount , price : $scope.updateamountdata.price* amount , dividend : $scope.updateamountdata.p_dividend * amount }
        ).then(function mySucces(response) {
            if (response.data == 'success') { 
                window.location.reload();
            }
        });
    }
    var glassuse;
    var glassproscript;
    //load updateprescription data
    $scope.openupdateprescription = function (ind) {
        $scope.updateprescriptiondata = $scope.cardata[ind];
        glassproscript = $scope.updateprescriptiondata;
        //撈特殊加工
        $http.post("../../frontendapi/CARloadspecial", { p_id : glassproscript.productid }
        ).then(function mySucces(response) {
            var special = response.data.loadproduct[0].special.split("|");
            $scope.CARloadprescription = [];
            for (var i in special) {
                if (special != null) {
                    $http.post("../../frontendapi/CARloadprescription", { id: special[i] , i : i }
                    ).then(function youSucces(response) {
                        $scope.CARloadprescription[response.data[0].i] = response.data[0];
                        //勾選
                        for (i in glassproscript.specialprocess) {
                            if (glassproscript.specialprocess[i] == $scope.CARloadprescription[response.data[0].i].p_processid) {
                                $scope.CARloadprescription[response.data[0].i].Checked = true;
                            } 
                        }
                    });
                }
            }
        });//撈完了
        if ($scope.updateprescriptiondata.hypermyopia == null) { glassuse = 'nochouse';$('#glass-prescription').hide();$('#special').hide();$('#glass-Remark').hide(); }
        else if ($scope.updateprescriptiondata.hypermyopia == 1) { glassuse = 'far'; $('#glass-prescription').show();$('#special').show(); $('#glass-Remark').show();}
        else if ($scope.updateprescriptiondata.hypermyopia == 2) { glassuse = 'near'; $('#glass-prescription').show(); $('#special').show(); $('#glass-Remark').show();}
        else if ($scope.updateprescriptiondata.hypermyopia == 3) { glassuse = 'twouse'; $('#glass-prescription').show(); $('#special').show(); $('#glass-Remark').show(); }
        document.getElementById("select-glass").value = glassuse;
        if (glassproscript.PD != null) { $('#p_PD').val(glassproscript.PD); }
        else { $('#p_PD').val(''); }
        if (glassproscript.rightOD != null) {
            var Rod = glassproscript.rightOD.split(" ");
            $('#p_RSPH').val(Rod[0]);
            $('#p_RCYL').val(Rod[1]);
            $('#p_RAX').val(Rod[2]);
        }
        else {
            $('#p_RSPH').val('');
            $('#p_RCYL').val('');
            $('#p_RAX').val('');
        }
        if (glassproscript.leftOD != null) {
            var Lod = glassproscript.leftOD.split(" ");
            $('#p_LSPH').val(Lod[0]);
            $('#p_LCYL').val(Lod[1]);
            $('#p_LAX').val(Lod[2]);
        }
        else {
            $('#p_LSPH').val('');
            $('#p_LCYL').val('');
            $('#p_LAX').val('');
        }
        if (glassproscript.remark != null) { $('#Remark').val(glassproscript.remark); }
        else { $('#Remark').val(''); }
    };

    $scope.updatecarprescription = function () {
        if ($('#select-glass').val() == 'nochouse') { alert('您的處方未修改完成'); }
        else if ($('#p_PD').val() == '' || $('#p_RSPH').val() == '' || $('#p_RCYL').val() == '' || $('#p_RAX').val() == '' || $('#p_LSPH').val() == '' || $('#p_LCYL').val() == '' || $('#p_LAX').val() == '') {
            alert('您的處方未修改完成');
        }
        else {
            //眼鏡種類
            var hypermyopia;
            if ($('#select-glass').val() == 'near') { hypermyopia = 1; }
            else if ($('#select-glass').val() == 'far') { hypermyopia = 2; }
            else if ($('#select-glass').val() == 'twouse') { hypermyopia = 3; }
            //特殊加工 
            var price = 0;
            var special = '|';
            for (i in $scope.CARloadprescription) {
                if ($scope.CARloadprescription[i].Checked == true) {
                    special += $scope.CARloadprescription[i].p_processid + '|';
                    price += $scope.CARloadprescription[i].amount;
                }
            }
            if (special == '|') { special = null; }
            $http.post("../../frontendapi/CARupdateprescription", {
                car_id : $scope.updateprescriptiondata.carid ,
                hypermyopia : hypermyopia,
                rightOD : $('#p_RSPH').val() + " " + $('#p_RCYL').val() + " " + $('#p_RAX').val(),
                leftOD: $('#p_LSPH').val() + " " + $('#p_LCYL').val() + " " + $('#p_LAX').val(),
                PD: $('#p_PD').val(),
                specialprocess: special,
                remark: $('#Remark').val(),
                totalprice: (price+$scope.updateprescriptiondata.price) * $scope.updateprescriptiondata.amount
            }
            ).then(function mySucces(response) {
                if (response.data == 'success') {
                    window.location.reload();
                }
            });
        }
    };
    $scope.deletecarprescription = function (carid) {
        if (confirm("確定將此商品從購物車移除??") == true) {
            $http.post("../../frontendapi/CARdelete", {
                car_id : carid ,
            }
            ).then(function mySucces(response) {
                if (response.data == 'success') {
                    alert("已成功刪除");
                    window.location.reload();
                }
            });
        } else {
            alert("已取消刪除");
        }
           
    };
    $scope.gotocheckout = function () {
        var check = 0;
        for (i in $scope.cardata) {
            if ($scope.cardata[i].prescription == 1 && $scope.cardata[i].hypermyopia == null) {
                check += 1;
            }
        }
        if (check == 0) { location.href = './checkOut'; }
        else { alert('您有 ' + check + ' 項產品處方未填寫'); }
    };
});
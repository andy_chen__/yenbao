﻿var app = angular.module('app', []);
app.controller('ctrl', function ($scope,$http) {
    //結帳頁，先撈資料，判斷身分，判斷處方填寫，送出結帳方法
    $http.post("../../frontendapi/checkoutload", { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] }
    ).then(function mySucces(response) {
        if (response.data.information[0]) {
            $scope.cardata = response.data.information;
            var check = 0;
            for (i in $scope.cardata) {
                if ($scope.cardata[i].prescription == 1 && $scope.cardata[i].hypermyopia == null) {
                    check += 1;
                }
            }
            if (check == 0) {
                $scope.totalprice = 0;
                for (i in $scope.cardata) {
                    $scope.totalprice += $scope.cardata[i].totalprice;
                }
            }
            else {
                location.href ='./onlineMall';
            }
            $('#name').val(response.data.address[0].name);
            $('#zip').val(response.data.address[0].postalcode);
            $('#address').val(response.data.address[0].city + response.data.address[0].township + response.data.address[0].address);
        }
        else {
            location.href = './onlineMall';
        }
        $scope.ordersend = function (totalprice) {
            if ($('#name').val() == '' || $('#zip').val() == '' || $('#address').val() == '') {
                alert('請輸入完整資料');
            }
            else {
                if (confirm("確定要送出此訂單??") == true) {
                    //送出資料
                    $http.post("../../frontendapi/checkoutinsert", { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] , totalprice : $scope.totalprice , name : $('#name').val() , zip : $('#zip').val() , address : $('#address').val() }
                    ).then(function mySucces(response) {
                        if (response.data) {
                            //修改購物車
                            var o_id = response.data;
                            for (var i = 0; i < $scope.cardata.length; i++) {
                                $http.post("../../frontendapi/turncar", { c_id : $scope.cardata[i].carid , o_id: o_id ,i:i }
                                ).then(function youSucces(response) {
                                    if (response.data.i == $scope.cardata.length - 1) {//送玩了
                                        alert("已成功送出");
                                        setTimeout(window.location.reload(), 3000);
                                        //連結
                                        var url = "./checkfinal";
                                        var newWindow = window.open(url, '_blank', 'PopUp');
                                        if (!newWindow) return false;
                                        var html = "";
                                        html += "<html><head></head><body><form id='formid' method='post' action='" + url + "'>";
                                        html += "<input type='text' name='o_id' value='" + o_id + "'/>";
                                        html += "<input type='text' name='totalprice' value='" + $scope.totalprice + "'/>";
                                        html += "<input type='text' name='name' value='" + $('#name').val() + "'/>";
                                        html += "</form><script type='text/javascript'>document.getElementById(\"formid\").submit()</script></body></html>";
                                        newWindow.document.write(html);
                                        return newWindow;
                                    }
                                });
                            }
                            
                        }
                    });
                }
                else {
                    alert("已取消送出");
                }

            }
        };
    });
})
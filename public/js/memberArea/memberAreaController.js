var app = angular.module('app', []);
app.controller('ctrl', function ($scope,$http) {
    $http.post("../../frontendapi/MEMBERload", { account: window.localStorage["userid"],pwd: window.localStorage["userpwd"] }
    ).then(function mySucces(response) {
        if (response.data.information[0]) {
            for (var i in response.data.information) {
                response.data.information[i].memberBirthday = new Date(response.data.information[i].memberBirthday).toLocaleDateString();
               }
            $scope.Memberdata = response.data;
            $('#twzipcode').twzipcode('set', { 'zipcode': $scope.Memberdata.information[0].memberPostcode });
        }
        else {
            location.href = './';
        }
    });
    $http.post("../../frontendapi/MEMBERloadhealth", { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] , num: 0 }
    ).then(function mySucces(response) {
        for (var i in response.data.health) {
            response.data.health[i].date = new Date(response.data.health[i].Stime).toLocaleDateString();
        }
        $scope.Memberhealth = response.data;
    });
    var P_order ='ORDER BY m_deal.date DESC';
    $http.post("../../frontendapi/MEMBERloadtransaction", { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] , num: 0 , order: P_order }
    ).then(function mySucces(response) {
        for (var i in response.data.transaction) {
            response.data.transaction[i].transactiontime = new Date(response.data.transaction[i].time).toLocaleDateString()+" "+ response.data.transaction[i].transactiontime;
        }
        if (response.data.transaction[0]) {
            $scope.transactionHistory = response.data.transaction;
        }
        else { $scope.transactionHistory = [{ 'transactionid' :-1 }];}
    });
    var N_Page = 0;
    $scope.HealthUPP = function () {
        if (N_Page > 0) {
            N_Page -= 9;
            $http.post("../../frontendapi/MEMBERloadhealth", { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] , num: N_Page }
            ).then(function mySucces(response) {
                for (var i in response.data.health) {
                    response.data.health[i].date = new Date(response.data.health[i].Stime).toLocaleDateString();
                }
                $scope.Memberhealth = response.data;
            });
        }
    }
    $scope.HealthDOWN = function () {
        N_Page += 9;
        $http.post("../../frontendapi/MEMBERloadhealth", { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] , num: N_Page }
        ).then(function mySucces(response) {
            if (response.data.health[0]) {
                for (var i in response.data.health) {
                    response.data.health[i].date = new Date(response.data.health[i].Stime).toLocaleDateString();
                }
                $scope.Memberhealth = response.data;
            }
            else {
                N_Page -= 9;
            }
        });
    }
    var P_Page = 0;
    $scope.transactionUPP = function () {
        if (P_Page > 0) {
            P_Page -= 3;
            $http.post("../../frontendapi/MEMBERloadtransaction", { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] , num: P_Page , order: P_order }
            ).then(function mySucces(response) {
                for (var i in response.data.transaction) {
                    response.data.transaction[i].transactiontime = new Date(response.data.transaction[i].time).toLocaleDateString() + " " + response.data.transaction[i].transactiontime;
                }
                $scope.transactionHistory = response.data.transaction;
            });
        }
    }
    $scope.transactionDOWN = function () {
        P_Page += 3;
        $http.post("../../frontendapi/MEMBERloadtransaction", { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] , num: P_Page , order: P_order }
        ).then(function mySucces(response) {
            if (response.data.transaction[0]) {
                for (var i in response.data.transaction) {
                    response.data.transaction[i].transactiontime = new Date(response.data.transaction[i].time).toLocaleDateString() + " " + response.data.transaction[i].transactiontime;
                }
                $scope.transactionHistory = response.data.transaction;
            }
            else {
                P_Page -= 3;
            }
        });
    }
    $scope.transactionOrder = function () {
        var order = $('#chooseorder').val();
        if (order == 'new') { P_order = 'ORDER BY m_deal.date DESC'; }
        else if (order == 'old') { P_order = 'ORDER BY m_deal.date ASC'; }
        else if (order == 'high') { P_order = 'ORDER BY m_deal.totalprice DESC'; }
        else if (order == 'low') { P_order = 'ORDER BY m_deal.totalprice ASC'; }
        $http.post("../../frontendapi/MEMBERloadtransaction", { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] , num: 0 , order: P_order }
        ).then(function mySucces(response) {
            for (var i in response.data.transaction) {
                response.data.transaction[i].transactiontime = new Date(response.data.transaction[i].time).toLocaleDateString() + " " + response.data.transaction[i].transactiontime;
            }
            $scope.transactionHistory = response.data.transaction;
        });
        P_Page = 0;
    }
    /*交易明細*/
    $scope.detailManager = function (id) {
        if ($scope.transactionHistory[id].visible) {
            $scope.transactionHistory[id].visible = false;
        } else {
            $scope.transactionHistory[id].visible = true;
        }
    }
    $scope.MemberUpdate = function () {
        $('#twzipcode').twzipcode('get', function (el) {
            var emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
            if ($('#m_update_email').val().search(emailRule) == -1) {
                alert("請確認電子郵件是否正確");
            }
            else if ($('#m_update_name').val() == ''||$('#m_update_phone').val() == ''|| $(el.zipcode).val() == ''||$('#m_update_address').val() == ''||$('#m_update_email').val() == '') {
                alert('您的資料不完整');
            }
            $http.post("../../frontendapi/MEMBERupdate", {
                'id': $scope.Memberdata.information[0].memberId,
                'name' : $('#m_update_name').val(),
                'phone' : $('#m_update_phone').val(),
                'city': $(el.county).val(),
                'township': $(el.district).val(),
                'postalcode': $(el.zipcode).val(),
                'address' : $('#m_update_address').val(),
                'email': $('#m_update_email').val()
            }
            ).then(function mySucces(response) {
                if (response.data == 'success') {
                    alert("成功編輯!!");
                    window.location.reload();
                }
            });
        });
    };
})
$(function () {
    $.post("../../frontendapi/MEMBERloadscreen",
             { account: window.localStorage["userid"], pwd: window.localStorage["userpwd"] }
    ).done(function (result) {
        for (var i in result.screen) {
            result.screen[i].date = new Date(result.screen[i].date).toLocaleDateString();
        }
        var screen = result.screen;
        var screenDate = [];
        var screenLeye = [];
        var screenReye = [];
        for (var i = 0 ; i < screen.length; i++) {
            screenDate[i] = screen[i].date;
            screenReye[i] = parseFloat(screen [i].rightvision) * -100;
            screenLeye[i] = parseFloat(screen[i].leftvision) * -100;
        }
        $('#container').highcharts({
            chart: {
                type: 'line'
            },
            title: {
                text: '視力紀錄'
            },
            xAxis: {
                categories: screenDate
            },
            yAxis: {
                title: {
                    text: '度數'
                }
            },
            plotOptions: {
                line: {
                    dataLabels: {
                        enabled: true
                    },
                    enableMouseTracking: false
                }
            },
            series: [{
                    name: '左眼視力',
                    data: screenLeye,
                    color: '#00ff00'
                }, {
                    name: '右眼視力',
                    data: screenReye,
                    color: '#0000ff'
                }]
        });
    });
});
($(function () {
    /*address*/
    $('#twzipcode').twzipcode({
        // 依序套用至縣市、鄉鎮市區及郵遞區號框
        'css': ['county', 'district', 'zipcode']
    });
}))
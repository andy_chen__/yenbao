﻿$(function () {
    //.parallax(xPosition, speedFactor, outerHeight) 設定選項:
    //xPosition - 水平位置
    //speedFactor - 滾動速度
    $('#img01').parallax("50%", 0.1);
    $('#img02').parallax("50%", 0.1);
    $('#img03').parallax("50%", 0.1);
});
var app = angular.module('App', []);
app.controller('aboutusCtrl', function ($scope, $http) { 
    $http.post("../../frontendapi/ABOUTUS", {num:0}
    ).then(function mySucces(response) {
        for (var i in response.data) {
            response.data[i].year = new Date(response.data[i].year).toLocaleDateString();
        }
        $scope.USYear = response.data;
    });
    var Page = 0;
    $scope.YearUPP = function () {
        if (Page > 0) {
            Page -= 6;
            $http.post("../../frontendapi/ABOUTUS", { num: Page }
            ).then(function mySucces(response) {
                for (var i in response.data) {
                    response.data[i].year = new Date(response.data[i].year).toLocaleDateString();
                }
                $scope.USYear = response.data;
            });
        }
    }
    $scope.YearDOWNP = function () { 
        Page += 6;
        $http.post("../../frontendapi/ABOUTUS", { num: Page }
        ).then(function mySucces(response) {
            if (response.data[0]) {
                for (var i in response.data) {
                    response.data[i].year = new Date(response.data[i].year).toLocaleDateString();
                }
                $scope.USYear = response.data;
            }
            else {
                Page -= 6;
            }
        });
    }
});
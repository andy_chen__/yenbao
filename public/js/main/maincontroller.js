﻿var app = angular.module('app', []);
app.controller('mainController',function ($scope, $http) {
    var QR = location.href;
    document.getElementById("QRimg").src='http://chart.apis.google.com/chart?cht=qr&chl=' + QR + '&chs=180x180';
    $http.get("../../frontendapi/HOMEhorse"
    ).then(function mySucces(response) {
        $scope.HOMEhorse = response.data;
    });
    $http.post("../../frontendapi/HOMEnews", {id:'0' , num:0}
    ).then(function mySucces(response) {
        for (var i in response.data) {
            response.data[i].newsDate = new Date(response.data[i].newsDate).toLocaleDateString();
        }
        $scope.HOMEnews = response.data;
    });
    $http.get("../../frontendapi/HOMEknowledge"
    ).then(function mySucces(response) {
        $scope.HOMEknowledge = response.data;
    });
    $http.get("../../frontendapi/HOMEproduce"
    ).then(function mySucces(response) {
        $scope.HOMEproduce = response.data;
    });
    $scope.NEWSclick = function (n_id) {
        $http.post("../../frontendapi/NEWSsearch", {id:n_id})
        .then(function mySucces(response) {
            if (response.data[0]) {
                response.data[0].articleDate = new Date(response.data[0].articleDate).toLocaleDateString();
            }
            $scope.NEWScontent = response.data;
            $('.newContain').html(($scope.NEWScontent[0].Article));
            $('#news-modal').modal('toggle');
        });
    }
    $scope.KNOWLEDGEclick = function (k_id) {
        window.localStorage["A_id"] = k_id;
        window.localStorage["articleType"] = "KNOWLEDGE";
    }
    //最新消息上下頁
    var N_Page = 0;
    $scope.NewsUPP = function () {
        if (N_Page > 0) {
            N_Page -= 3;
            $http.post("../../frontendapi/HOMEnews", { id: '0' ,num: N_Page }
            ).then(function mySucces(response) {
                for (var i in response.data) {
                    response.data[i].newsDate = new Date(response.data[i].newsDate).toLocaleDateString();
                }
                $scope.HOMEnews = response.data;
            });
        }
    }
    $scope.NewsDOWNP = function () {
        N_Page += 3;
        $http.post("../../frontendapi/HOMEnews", { id: '0' , num: N_Page }
        ).then(function mySucces(response) {
            if (response.data[0]) {
                for (var i in response.data) {
                    response.data[i].newsDate = new Date(response.data[i].newsDate).toLocaleDateString();
                }
                $scope.HOMEnews = response.data;
            }
            else {
                N_Page -= 3;
            }
        });
    }
    $scope.gotoherf = function (h) {
        if (h == '') { }
        else {
            top.location.href = h;
        }
    }

    $scope.SearchProduct = function () {
        var SPid = $('#SearchPid').val();
        if (SPid.substring(0, 1) == 'P') {
            var SSPid = SPid.substring(1, SPid.length);
            $http.post("../../frontendapi/HOMEserachproduct", { id: SSPid })
            .then(function mySucces(response) {
                if (response.data == 'noid') {
                    alert('查無此編號 ' + SPid + ' 產品');
                }
                else {
                    location.href = './productContent?id='+ response.data.id;
                }
            });
        }
        else {
            alert('搜尋格式需為 " P+產品編號 " ');
        }
    }
    var dt = new Date();
    var yearnum, monthnum,daynum;
    yearnum = dt.getFullYear();
    monthnum = (dt.getMonth() + 1);
    daynum = dt.getDate();
    if (monthnum / 10 < 1) { monthnum = '0' + monthnum; }
    var todayDate = yearnum + "-" + monthnum + "-" + daynum;
    console.log(todayDate);
    if (window.localStorage["views"] == todayDate) { }
    else {
        $http.post("../../frontendapi/Viewsinsert")
            .then(function mySucces(response) {
            if (response.data == 'success') {
                window.localStorage["views"] = todayDate;
            }
        });
    }
    
})

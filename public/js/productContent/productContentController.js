﻿ /*
 Credits:
 https://github.com/marcaube/bootstrap-magnify
 */
 !function ($) {

     "use strict"; // jshint ;_;

     var Magnify = function (element, options) {
         this.init('magnify', element, options)
     }

     Magnify.prototype = {

         constructor: Magnify

         , init: function (type, element, options) {
             var event = 'mousemove'
                 , eventOut = 'mouseleave';

            this.type = type;
            this.$element = $(element);
            this.options = this.getOptions(options);
            this.nativeWidth = 0;
            this.nativeHeight = 0;

             this.$element.wrap('<div class="magnify" \>');
             this.$element.parent('.magnify').append('<div class="magnify-large" \>');
             this.$element.siblings(".magnify-large").css("background", "url('" + this.$element.attr("src") + "') no-repeat");
             this.$element.parent('.magnify').on(event + '.' + this.type, $.proxy(this.check, this));
             this.$element.parent('.magnify').on(eventOut + '.' + this.type, $.proxy(this.check, this));
         }

         , getOptions: function (options) {
             options = $.extend({}, $.fn[this.type].defaults, options, this.$element.data())

             if (options.delay && typeof options.delay == 'number') {
                 options.delay = {
                     show: options.delay
                     , hide: options.delay
                 }
             }

             return options
         }

        , check: function (e) {
            
             var container = $(e.currentTarget);
             var self = container.children('img');
             var mag = container.children(".magnify-large");

             // Get the native dimensions of the image
             if (!this.nativeWidth && !this.nativeHeight) {
                 var image = new Image();
                 image.src = self.attr("src");

                 this.nativeWidth = image.width;
                 this.nativeHeight = image.height;

             } else {

                 var magnifyOffset = container.offset();
                 var mx = e.pageX - magnifyOffset.left;
                 var my = e.pageY - magnifyOffset.top;

                 if (mx < container.width() && my < container.height() && mx > 0 && my > 0) {
                     mag.fadeIn(100);
                 } else {
                     mag.fadeOut(100);
                 }

                 if (mag.is(":visible")) {
                     var rx = Math.round(mx / container.width() * this.nativeWidth - mag.width() / 2) * -1;
                     var ry = Math.round(my / container.height() * this.nativeHeight - mag.height() / 2) * -1;
                     var bgp = rx + "px " + ry + "px";

                     var px = mx - mag.width() / 2;
                     var py = my - mag.height() / 2;

                     mag.css({ left: px, top: py, backgroundPosition: bgp });
                 }
             }

         }
     }


     /* MAGNIFY PLUGIN DEFINITION
      * ========================= */

     $.fn.magnify = function (option) {
         return this.each(function () {
             var $this = $(this)
                 , data = $this.data('magnify')
                 , options = typeof option == 'object' && option
             if (!data) $this.data('tooltip', (data = new Magnify(this, options)))
             if (typeof option == 'string') data[option]()
         })
     }

     $.fn.magnify.Constructor = Magnify

     $.fn.magnify.defaults = {
         delay: 0
     }


     /* MAGNIFY DATA-API
      * ================ */

     $(window).on('load', function () {
         $('[data-toggle="magnify"]').each(function () {
             var $mag = $(this);
             $mag.magnify()
         })
     })

} (window.jQuery);
$(document).ready(function () {
    $('[data-toggle="tooltip"]').tooltip();   
    $('#shopcarIcon').tooltip('show');
    setTimeout(function () {
        $('#shopcarIcon').tooltip('hide');
    }, 3000);
});
    
var app = angular.module('app', []);
app.controller('productCtrl', function ($scope, $http) {
    $scope.QRhref = location.href;
    $http.post("../../frontendapi/PRODUCTcontent", { p_id: p_id }
    ).then(function mySucces(response) {
        for (var i in response.data.qa) {
            response.data.qa[i].QAdate = new Date(response.data.qa[i].date).toLocaleDateString()+" "+ response.data.qa[i].QAdate;
            if (response.data.qa[i].replytime != null) {
                response.data.qa[i].QAreplytime = new Date(response.data.qa[i].replytime).toLocaleDateString()+" "+ response.data.qa[i].QAreplytime;
            }
        }
        $scope.PRODUCTcontent = response.data;
        //加入單位
        $scope.PRODUCTcontent.loaddata[0].Pstock += '個';
        for (var i in $scope.PRODUCTcontent.qa) {
            if ($scope.PRODUCTcontent.qa[i].QAmemberid != null) { 
            $http.post("../../frontendapi/PRODUCTqaPic", { id: $scope.PRODUCTcontent.qa[i].QAmemberid ,  i : i }
                ).then(function youSucces(response) {
                    $scope.PRODUCTcontent.qa[response.data[0].i].QApicture = response.data[0].QApicture;
            });
            }
        }
        $('#previewcontent').html($scope.PRODUCTcontent.loaddata[0].Pcontent);
        $scope.PRODUCTcaramount = 1;
        document.getElementById("singlebutton").disabled = false;
        if ($scope.PRODUCTcontent.loaddata[0].Pstock == 0) {
            $scope.PRODUCTcaramount = 0;
            document.getElementById("singlebutton").disabled = true;
        }
        else if ($scope.PRODUCTcontent.loaddata[0].statue == 0) {
            $scope.PRODUCTcaramount = 0;
            document.getElementById("singlebutton").disabled = true;
            $scope.PRODUCTcontent.loaddata[0].Pstock = '未上架';
        }
    });
    $scope.quantityNumberD = function () {
        if ($scope.PRODUCTcaramount == 1||$scope.PRODUCTcaramount == 0) { }
        else { $scope.PRODUCTcaramount -= 1; }
    };
    $scope.quantityNumberU = function () {
        if ($scope.PRODUCTcaramount == $scope.PRODUCTcontent.loaddata[0].Pstock || $scope.PRODUCTcaramount == 0) { }
        else { $scope.PRODUCTcaramount += 1; }
    };
    //QA會員登入
    $http.post("./frontendapi/LOGIN", { id : window.localStorage["userid"] , pwd : window.localStorage["userpwd"] }
    ).then(function mySucces(response) {
        $scope.LOGINdata = response.data;
        if ($scope.LOGINdata == 'nologin') {
            window.localStorage["memberid"] = '';
            window.localStorage["membername"] ='';
            $scope.qalogin = 'nologin';
        } 
        else {
            window.localStorage["memberid"] = $scope.LOGINdata[0].id;
            window.localStorage["membername"] = $scope.LOGINdata[0].name;
            $scope.qalogin = $scope.LOGINdata[0].id;
        }
    });
    $scope.MEMBERsendQA = function () {
        if ($('#MEMBERsendQAcomtent').val() != '') {
            $http.post("../../frontendapi/MEMBERinsertQA", {
                m_id : window.localStorage["memberid"], 
                m_name: window.localStorage["membername"], 
                p_id: p_id, 
                content: $('#MEMBERsendQAcomtent').val()
            }
            ).then(function mySucces(response) {
                if (response.data == 'success insert') {
                    alert("留言已成功送出");
                    window.location.reload();
                }
            });
        }
        else {
            alert("請輸入留言");
        }
    };
    $scope.noMEMBERsendQA = function () {
        if ($('#noMEMBERsendQAname').val() != '' && $('#noMEMBERsendQAemail').val() != '' && $('#noMEMBERsendQAphone').val() != '' && $('#noMEMBERsendQAcomtent').val() != '') {
            var emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
            if ($('#noMEMBERsendQAemail').val().search(emailRule) == -1) {
                alert("請確認您的電子郵件是否正確");
            } else {
                $http.post("../../frontendapi/noMEMBERinsertQA", {
                    name: $('#noMEMBERsendQAname').val(), 
                    email: $('#noMEMBERsendQAemail').val(), 
                    phone: $('#noMEMBERsendQAphone').val(), 
                    p_id: p_id, 
                    content: $('#noMEMBERsendQAcomtent').val()
                }
                ).then(function mySucces(response) {
                    if (response.data == 'success insert') {
                        alert("留言已成功送出");
                        window.location.reload();
                    }
                });
            }
        }
        else {
            alert('您的資料不完整，無法送出');
        }
    };
    $scope.InsertShoppingCar = function (p_id) {
        $http.post("./frontendapi/LOGIN", { id: window.localStorage["userid"], pwd: window.localStorage["userpwd"] }
        ).then(function mySucces(response) {
            $scope.LOGINdata = response.data;
            if ($scope.LOGINdata == 'nologin') {
                $('#login-modal').modal();
                window.localStorage["memberid"] = '';
            }
            else {
                window.localStorage["memberid"] = $scope.LOGINdata[0].id;
                $http.post("./frontendapi/CARinsert", {
                    memberid : $scope.LOGINdata[0].id ,
                    productid : p_id,
                    amount : $scope.PRODUCTcaramount,
                    prescription: $scope.PRODUCTcontent.loaddata[0].Pprescription,
                    totalprice : $scope.PRODUCTcaramount * $scope.PRODUCTcontent.loaddata[0].Pprice ,
                    dividend: $scope.PRODUCTcaramount * $scope.PRODUCTcontent.loaddata[0].Pdividend
                }
                ).then(function youSucces(response) {
                    if (response.data == 'success') {
                        alert('以加到購物車');
                        window.location.reload();
                    }
                });
            }
        });
    }

});
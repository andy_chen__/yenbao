﻿var app = angular.module('App', []);
app.controller('knowledgeCtrl', function ($scope, $http) {
    //知識分享
    $http.get("../../frontendapi/KNOWLEDGEloadtype"
    ).then(function mySucces(response) {
        $scope.KNOWLEDGEtype = response.data;
    });
    $('#searchtextS').val(k_search);
    $('#searchtextB').val(k_search);
    var k_typedata;
    if (k_type == '') { k_typedata = ''; }
    else { k_typedata = 'and o_knowledge.o_k_type =' + k_type;}
    $http.post("../../frontendapi/KNOWLEDGEload" , { type : k_typedata , data : k_search  , k_num : 0 }
    ).then(function mySucces(response) {
        for (var i in response.data) {
            response.data[i].knowDate = new Date(response.data[i].Time).toLocaleDateString();
        }
        $scope.KNOWLEDGEload = response.data;
    });
    $scope.searchtype = function (t) {
        location.href = "./knowledgeSharing?type=" + t + "&search="+ k_search;
    };
    $scope.searchtextS = function () {
        location.href = "./knowledgeSharing?type=" + k_type + "&search=" + $('#searchtextS').val();
    };
    $scope.searchtextB = function () {
        location.href = "./knowledgeSharing?type=" + k_type + "&search=" + $('#searchtextB').val();
    };
    //上下頁
    var Page = 0;
    $scope.KNOWLEDGEUPP = function () {
        if (Page > 0) {
            Page -= 5;
            $http.post("../../frontendapi/KNOWLEDGEload" , { type : k_typedata , data : k_search  , k_num : Page }
            ).then(function mySucces(response) {
                for (var i in response.data) {
                    response.data[i].knowDate = new Date(response.data[i].Time).toLocaleDateString();
                }
                $scope.KNOWLEDGEload = response.data;
            });
        }
    }
    $scope.KNOWLEDGEDOWN = function () {
        Page += 5;
        $http.post("../../frontendapi/KNOWLEDGEload" , { type : k_typedata , data : k_search  , k_num : Page }
        ).then(function mySucces(response) {
            if (response.data[0]) {
                for (var i in response.data) {
                    response.data[i].knowDate = new Date(response.data[i].Time).toLocaleDateString();
                }
                $scope.KNOWLEDGEload = response.data;
            }
            else {
                Page -= 5;
            }
        });
    }
});
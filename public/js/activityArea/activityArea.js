﻿var eventData = [];
var app = angular.module('App', []);
app.controller('activityAreaCtrl', function ($scope, $http) {
    
    //活動消息
    $http.get("../../frontendapi/ACTIVITYloadyear"
    ).then(function mySucces(response) {
        $scope.ACTIVITYyear = response.data;
    });
    $('#ACTIVITYserachtext').val(a_search);

    $http.post("../../frontendapi/ACTIVITYload", {date: a_date , search : a_search ,a_num:0}
    ).then(function mySucces(response) {
        for (var i in response.data) {
            response.data[i].activityDate = new Date(response.data[i].Time).toLocaleDateString();
        }
        $scope.ACTIVITYload = response.data;
        calendarReady(response.data);
    });
    $scope.ACTIVITYsearchyear = function (a_year) {
        location.href = "./activityArea?date=" + a_year + "&search=" + a_search;
    }
    $scope.ACTIVITYserachdata = function () {
        location.href = "./activityArea?date=" + a_date + "&search=" + $('#ACTIVITYserachtext').val();
    }

    //上下頁
    var Page = 0;
    $scope.ACTIVITYUPP = function () {
        if (Page > 0) {
            Page -= 5;
            $http.post("../../frontendapi/ACTIVITYload", { date: a_date , search : a_search , a_num: 0 }
            ).then(function mySucces(response) {
                for (var i in response.data) {
                    response.data[i].activityDate = new Date(response.data[i].Time).toLocaleDateString();
                }
                $scope.ACTIVITYload = response.data;
            });
        }
    }
    $scope.ACTIVITYDOWN = function () {
        Page += 5;
        $http.post("../../frontendapi/ACTIVITYload", { date: a_date , search : a_search , a_num: 0 }
        ).then(function mySucces(response) {
            if (response.data[0]) {
                for (var i in response.data) {
                    response.data[i].activityDate = new Date(response.data[i].Time).toLocaleDateString();
                }
                $scope.ACTIVITYload = response.data;
            }
            else {
                Page -= 5;
            }
        });
    }
});

function calendarReady(data){

    for (var i in data){
        var jsonTemp={};
        jsonTemp.date=getFormattedDate(new Date(data[i].activityDate));
        jsonTemp.badge=false;
        jsonTemp.title=data[i].activityTitle;
        eventData.push(jsonTemp);
    }
    $("#my-calendar").zabuto_calendar({
        data:eventData,
        language: "en", 
        action: function () {
            var time = this.id.split('_')[3];
            location.href = "./activityArea?date=" + time + "&search=" + a_search;
        }
    });
}

function getFormattedDate(date) {
  var year = date.getFullYear();
  var month = (1 + date.getMonth()).toString();
  month = month.length > 1 ? month : '0' + month;
  var day = date.getDate().toString();
  day = day.length > 1 ? day : '0' + day;
  return year+'-'+month + '-' + day;
}
// JavaScript Document

$(function () {
    var date = new Date();
    //$('#dateRange_input').val(date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate() + '-' + date.getFullYear() + '/' + (date.getMonth() + 1) + '/' + date.getDate());
    $('#dateRange_btn').daterangepicker({
        "autoApply": true,
        "startDate": (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear() + '-' + (date.getMonth()+1) + '/' + date.getDate() + '/' + date.getFullYear(),
        "endDate": (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear() + '-' + (date.getMonth() + 1) + '/' + date.getDate() + '/' + date.getFullYear(),
    }, function (start, end, label) {
        $('#dateRange_input').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
        
    });
   
});



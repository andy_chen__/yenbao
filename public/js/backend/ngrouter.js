﻿var ngPages = angular.module('NgPages', ['ngRoute']);
ngPages.config(['$routeProvider', '$locationProvider', function ($routeProvider, $locationProvider) {
        if (c_type == 1 && c_article ==1) {
            $routeProvider
		    //Top Menu-Company Profile
		    .when('/companyProfile', {
                templateUrl: './pages/companyProfile.html'
            })

	    	//Top Menu-Company Profile
		    .when('/messageManagement', {
                templateUrl: './pages/messageManagement.html'
            })

            //Top Menu-Company Profile
		    .when('/ordersManagement', {
                templateUrl: './pages/ordersManagement.html'
            })

	    	//Top Menu-Company Profile
		    .when('/viewAll', {
                templateUrl: './pages/viewAll.html'
            })

	    	//Sidebar Menu-Branch Management
	    	.when('/branchManagement', {
                templateUrl: './pages/branchManagement.html'
            })

	    	//Sidebar Menu-Member Management
	    	.when('/memberManagement', {
                templateUrl: './pages/memberManagement.html'
            })

	    	//Sidebar Menu-Carousel Management
	    	.when('/carouselManagement', {
                templateUrl: './pages/carouselManagement.html'
            })

	    	//Sidebar Menu-News Management
	    	.when('/newsManagement', {
                templateUrl: './pages/newsManagement.html'
            })

	    	//Sidebar Menu-Activity Management
	    	.when('/activityManagement', {
                templateUrl: './pages/activityManagement.html'
            })

	    	//Sidebar Menu-Product Management
	    	.when('/productManagement', {
                templateUrl: './pages/productManagement.html'
            })

		    //Sidebar Menu-Knowledge Management
		    .when('/knowledgeManagement', {
                templateUrl: './pages/knowledgeManagement.html'
            })

		    //Sidebar Menu-Knowledge Management
		    .when('/analysisManagement', {
                templateUrl: './pages/analysisManagement.html'
            })

	    	//Sidebar Menu-Knowledge Management
	    	.when('/codeReader', {
                templateUrl: './pages/codeReader.html'
            })

		    .otherwise({
                redirectTo: '/analysisManagement'
            });
        }
        else if (c_type == 1 && c_article == 0) {
            $routeProvider
		    //Top Menu-Company Profile
		    .when('/companyProfile', {
                templateUrl: './pages/companyProfile.html'
            })

	    	//Top Menu-Company Profile
		    .when('/messageManagement', {
                templateUrl: './pages/messageManagement.html'
            })

            //Top Menu-Company Profile
		    .when('/ordersManagement', {
                templateUrl: './pages/ordersManagement.html'
            })

	    	//Top Menu-Company Profile
		    .when('/viewAll', {
                templateUrl: './pages/viewAll.html'
            })

	    	//Sidebar Menu-Branch Management
	    	.when('/branchManagement', {
                templateUrl: './pages/branchManagement.html'
            })

	    	//Sidebar Menu-Member Management
	    	.when('/memberManagement', {
                templateUrl: './pages/memberManagement.html'
            })

	    	//Sidebar Menu-Carousel Management
	    	.when('/carouselManagement', {
                templateUrl: './pages/carouselManagement.html'
            })

	    	//Sidebar Menu-News Management
	    	.when('/newsManagement', {
                templateUrl: './pages/newsManagement.html'
            })

	    	//Sidebar Menu-Activity Management
	    	//.when('/activityManagement', {
            //    templateUrl: './pages/activityManagement.html'
            //})

	    	//Sidebar Menu-Product Management
	    	.when('/productManagement', {
                templateUrl: './pages/productManagement.html'
            })

		    //Sidebar Menu-Knowledge Management
		    //.when('/knowledgeManagement', {
            //    templateUrl: './pages/knowledgeManagement.html'
            //})

		    //Sidebar Menu-Knowledge Management
		    .when('/analysisManagement', {
                templateUrl: './pages/analysisManagement.html'
            })

	    	//Sidebar Menu-Knowledge Management
	    	.when('/codeReader', {
                templateUrl: './pages/codeReader.html'
            })

		    .otherwise({
                redirectTo: '/analysisManagement'
            });
        }
        else if (c_article == 1) {
            $routeProvider
		    //Top Menu-Company Profile
		    .when('/companyProfile', {
                templateUrl: './pages/companyProfile.html'
            })
            //Sidebar Menu-News Management
	    	.when('/newsManagement', {
                templateUrl: './pages/newsManagement.html'
            })
            //Sidebar Menu-Member Management
            .when('/memberManagement', {
                templateUrl: './pages/memberManagement.html'
            })
            //Sidebar Menu-Activity Management
	    	.when('/activityManagement', {
                templateUrl: './pages/activityManagement.html'
            })
		    //Sidebar Menu-Knowledge Management
		    .when('/knowledgeManagement', {
                templateUrl: './pages/knowledgeManagement.html'
            })
            .otherwise({
                redirectTo: '/companyProfile'
            });
        }
        else {
            $routeProvider
		    //Top Menu-Company Profile
		    .when('/companyProfile', {
                templateUrl: './pages/companyProfile.html'
            })
            //Sidebar Menu-News Management
	    	.when('/newsManagement', {
                templateUrl: './pages/newsManagement.html'
            })
            //Sidebar Menu-Member Management
            .when('/memberManagement', {
                templateUrl: './pages/memberManagement.html'
            })
            .otherwise({
                redirectTo: '/companyProfile'
            });
        }
    }]);
ngPages.controller('backendController', function ($scope, $http) {
    $scope.BB_name = B_name + '/' + c_name;
    $scope.BB_id = c_id;
    var BIGERQAnum = 10;
    function QAmessageREload() {
        //載入上方QA通知
        $http.post("./backend/message", { type: c_type }
        ).then(function mySucces(response) {
            for (var i in response.data.QAmessage) {
                response.data.QAmessage[i].QAdate = new Date(response.data.QAmessage[i].QAdate).toLocaleDateString();
                response.data.QAmessage[i].QAdate = (response.data.QAmessage[i].QAdate).replace("-", "/");
                response.data.QAmessage[i].QAdate = (response.data.QAmessage[i].QAdate).replace("-", "/");
            }
            $scope.QAmessage = response.data.QAmessage;
            $scope.QAnum = response.data.tic[0].NUM;
            if ($scope.QAnum == 0) {
                $('#QABmessage').hide();
                $('#QASmessage').hide();
            }
            else {
                $('#QABmessage').show();
                $('#QASmessage').show();
                $('#QABmessage').html($scope.QAnum);
                $('#QASmessage').html($scope.QAnum);
            }
        });
        //載入上方QA通知 END
        //載入大頁的QA通知
        $http.post("./backend/BIGERmessage", { num: BIGERQAnum }
        ).then(function mySucces(response) {
            for (var i in response.data.QAmessage) {
                response.data.QAmessage[i].QAtime = new Date(response.data.QAmessage[i].QAdate).toLocaleTimeString();
                response.data.QAmessage[i].QAdate = new Date(response.data.QAmessage[i].QAdate).toLocaleDateString();
                response.data.QAmessage[i].QAdate = (response.data.QAmessage[i].QAdate).replace("-", "/");
                response.data.QAmessage[i].QAdate = (response.data.QAmessage[i].QAdate).replace("-", "/");
            }
            $scope.BIGERQAmessage = response.data.QAmessage;
        });
        //載入大頁的QA通知 END
        setTimeout(QAmessageREload, 60000);
    };
    if (c_type == 1) {
        QAmessageREload();
    }
    $scope.BIGERQAmessageloadmore = function () {
        BIGERQAnum += 10;
        //載入大頁的QA通知
        $http.post("./backend/BIGERmessage", { num: BIGERQAnum }
        ).then(function mySucces(response) {
            for (var i in response.data.QAmessage) {
                response.data.QAmessage[i].QAtime = new Date(response.data.QAmessage[i].QAdate).toLocaleTimeString();
                response.data.QAmessage[i].QAdate = new Date(response.data.QAmessage[i].QAdate).toLocaleDateString();
                response.data.QAmessage[i].QAdate = (response.data.QAmessage[i].QAdate).replace("-", "/");
                response.data.QAmessage[i].QAdate = (response.data.QAmessage[i].QAdate).replace("-", "/");
            }
            $scope.BIGERQAmessage = response.data.QAmessage;
        });
        //載入大頁的QA通知 END
    };
    $scope.QAmessageload = function () {
        //載入上方QA通知
        $http.post("./backend/message", { type: c_type }
        ).then(function mySucces(response) {
            for (var i in response.data.QAmessage) {
                response.data.QAmessage[i].QAdate = new Date(response.data.QAmessage[i].QAdate).toLocaleDateString();
                response.data.QAmessage[i].QAdate = (response.data.QAmessage[i].QAdate).replace("-", "/");
                response.data.QAmessage[i].QAdate = (response.data.QAmessage[i].QAdate).replace("-", "/");
            }
            $scope.QAmessage = response.data.QAmessage;
            $scope.QAnum = response.data.tic[0].NUM;
            if ($scope.QAnum == 0) {
                $('#QABmessage').hide();
                $('#QASmessage').hide();
            }
            else {
                $('#QABmessage').show();
                $('#QASmessage').show();
                $('#QABmessage').html($scope.QAnum);
                $('#QASmessage').html($scope.QAnum);
            }
        });
        //載入上方QA通知 END
        //載入大頁的QA通知
        $http.post("./backend/BIGERmessage", { num: BIGERQAnum }
        ).then(function mySucces(response) {
            for (var i in response.data.QAmessage) {
                response.data.QAmessage[i].QAtime = new Date(response.data.QAmessage[i].QAdate).toLocaleTimeString();
                response.data.QAmessage[i].QAdate = new Date(response.data.QAmessage[i].QAdate).toLocaleDateString();
                response.data.QAmessage[i].QAdate = (response.data.QAmessage[i].QAdate).replace("-", "/");
                response.data.QAmessage[i].QAdate = (response.data.QAmessage[i].QAdate).replace("-", "/");
            }
            $scope.BIGERQAmessage = response.data.QAmessage;
        });
        //載入大頁的QA通知 END
    };
    
    $scope.QAclick = function (id,name,type,price) {
        //OA開啟
        $("#overlayComment").removeClass("hide");
        $(document.body).addClass("no-scroll");
        var QA_p_id = "P" + id;
        $('#QA_p_id').val(QA_p_id);
        $('#qaP_id').html("商品貨號：" + QA_p_id);
        $('#qaP_name').html("商品名稱：" + name);
        $('#qaP_type').html("商品類別：" + type);
        $('#qaP_price').html("商品價格：" + price);
        $.ajax({
            type: "POST",
            url: "./productManagementapi/qaload",
            data: { 'ProductId': QA_p_id },
            dataType: "json"
        }).done(function (sresult) {
            $("#productCommentGrid").jsGrid({
                
                height: 340,
                width: "100%",
                
                //Functions
                editing: true,
                sorting: true,
                paging: true,
                autoload: true,
                pageSize: 10,
                pageButtonCount: 5,
                deleteConfirm: "確定刪除這筆資料?",
                onItemDeleted: function (args) {
                    $.ajax({
                        type: "POST",
                        url: "./productManagementapi/qadelete",
                        data: { 'id': args.item.QAid },
                        dataType: "json"
                    });
                },
                rowClick: function (args) {
                    $('#myCommentModal').modal('show');
                    var QA_id = args.item.QAid;
                    $('#QA_id').val(args.item.QAid);
                    $('#QAquestion').val(args.item.QAquestion);
                    $('#QArequest').val(args.item.QArequest);
                },
                
                //Data
                data: sresult,
                
                //Fields
                fields: [
                    { name: "QAdate", title: "留言日期", type: "text", width: 120, align: "center", editing: false },
                    { name: "memberid", title: "會員編號", type: "text", width: 80, align: "center", editing: false },
                    { name: "QAname", title: "姓名", type: "text", width: 100, align: "center", editing: false },
                    { name: "QAreply", type: "checkbox", title: "回覆", sorting: true, editing: false },
                    { name: "QAemail", title: "信箱", type: "text", width: 150, align: "center", editing: false },
                    { name: "QAphone", title: "電話", type: "text", width: 120, align: "center", editing: false },
                    { type: "control", editButton: false }
                ]
            });
        });
    };
    $http.get("./analysisManagementapi/saletopproduct"
    ).then(function mySucces(response) {
        $scope.BestSales = response.data; 
    });
    $http.get("./analysisManagementapi/BarChartConpany"
    ).then(function mySucces(response) {
        $scope.BarChartConpany = response.data;
    });
    $http.get("./analysisManagementapi/BarChartType"
    ).then(function mySucces(response) {
        $scope.BarChartType = response.data;
    });
});

ngPages.controller('idController', function ($scope) {
    $scope.title = "ID Management";
});

ngPages.controller('memberController', function ($scope) {
    $scope.title = "Member Management";
});

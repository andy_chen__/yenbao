$(function () {
    //scrollable sidebar
    $('.scrollable-sidebar').slimScroll({
        
        size: '0px'
    });
    
    
    //Collapsible Sidebar Menu
    $('.sidebar-menu .openable > a').click(function () {
        
        if (!$('aside').hasClass('sidebar-mini') || Modernizr.mq('(max-width: 991px)')) {
            if ($(this).parent().children('.submenu').is(':hidden')) {
                $(this).parent().siblings().removeClass('open').children('.submenu').slideUp(200);
                $(this).parent().addClass('open').children('.submenu').slideDown(200);
            }
            else {
                $(this).parent().removeClass('open').children('.submenu').slideUp(200);
            }
        }
        
        return false;
    });
    
    
    //Open active menu
    if (!$('.sidebar-menu').hasClass('sidebar-mini') || Modernizr.mq('(max-width: 767px)')) {
        $('.openable.open').children('.submenu').slideDown(200);
    }
    
    
    //Humburger Icon For Computer
    $('#sidebarToggleLG').click(function () {
        $('aside').toggleClass('sidebar-mini');
        $('.main-container').toggleClass('sidebar-mini');
							
    });
    
    
    //Humburger Icon For Pad/Phone
    $('#sidebarToggleSM').click(function () {
        $('.wrapper').toggleClass('display-left');
    });
    
    $('.dropdown-menu input').click(function (e) {
        e.stopPropagation();
        //This will prevent the event from bubbling up and close the dropdown when you type/click on text boxes.
    });
    
    
    //Close Menu
    $('.main-menu ul a').click(function () {
        hideMenu();
    });
    
    function hideMenu() {
        
        if (!$('#mAside').hasClass('sidebar-mini')) {
            //Computer
            $('aside').toggleClass('sidebar-mini');
            $('.main-container').toggleClass('sidebar-mini');
        }
        //Smart Phone
        $('.wrapper').toggleClass('display-left');
    }
    //更新聯絡我們
    function messageload() {
        $.ajax({
            type: "GET",
            url: "./messageManagementapi/loadnoread",
            data: '',
            dataType: "json"
        }).done(function (result) {
            if (result[0].contentusReadyetNUM == 0) {
                $('#noreadmessage').hide();
                $('#Smessage').hide();
            }
            else {
                $('#noreadmessage').show();
                $('#Smessage').show();
            }
            $('#noreadmessage').html(result[0].contentusReadyetNUM);
            $('#Smessage').html(result[0].contentusReadyetNUM);
        });
        setTimeout(messageload,60000);
    };
    if (c_type == 1) {messageload(); }
    
    //更新訂單
    function ordersload() {
        $.ajax({
            type: "GET",
            url: "./ordersManagementapi/LoadOrdersNoPay",
            data: '',
            dataType: "json"
        }).done(function (result) {
            if (result[0].NUM == 0) {
                $('#noreadorders').hide();
                $('#Sorders').hide();
            }
            else {
                $('#noreadorders').show();
                $('#Sorders').show();
            }
            $('#noreadorders').html(result[0].NUM);
            $('#Sorders').html(result[0].NUM);
        });
        setTimeout(ordersload, 60000);
    };
    if (c_type == 1) { ordersload(); }
    
    
    //判斷權限開啟按鈕
    //總店&分店
    if (c_type == 1) {
        $('#SQAmessagefromN').show();
        $('#SmessagefromM').show();
        $('#SordersfromM').show();
        $('#Bmessagefrom').show();
        $('#branchfrom').show();
        $('#memberfrom').show();
        $('#carouselfrom').show();
        $('#newsfrom').show();
        $('#productfrom').show();
        $('#analysisfrom').show();
    }
    else {
        $('#SQAmessagefromN').hide();
        $('#SmessagefromM').hide();
        $('#SordersfromM').hide();
        $('#Bmessagefrom').hide();
        $('#branchfrom').hide();
        $('#memberfrom').show();
        $('#carouselfrom').hide();
        $('#newsfrom').show();
        $('#productfrom').hide();
        $('#analysisfrom').hide();
    }
    //是否貼文
    if (c_article == 1) {
        $('#activityfrom').show();
        $('#knowledgefrom').show();
    }
    else if (c_article == 0) {
        $('#activityfrom').hide();
        $('#knowledgefrom').hide();
    }
});

﻿var app = angular.module('app', []);
app.controller('ctrl', function ($scope,$http) {
    var code;
    $scope.createCode=function() {
        code = "";
        var codeLength = 6; //验证码的长度
        var checkCode = document.getElementById("checkCode");
        var codeChars = new Array(0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z'); //所有候选组成验证码的字符，当然也可以用中文的
        for (var i = 0; i < codeLength; i++) {
            var charNum = Math.floor(Math.random() * 52);
            code += codeChars[charNum];
        }
        if (checkCode) {
            checkCode.className = "code";
            checkCode.innerHTML = code;
        }
    }
    $scope.validateCode=function() {
        var inputCode = document.getElementById("inputCode").value;
        if (inputCode.length <= 0) {
            alert("請輸入驗證碼！");
            return 1;
        }
        else if (inputCode.toUpperCase() != code.toUpperCase()) {
            alert("驗證碼輸入有誤！");
            $scope.createCode();
            document.getElementById("inputCode").value = "";
            return 2;
        }
        else {
            return 0;
        }
    }
    $scope.submit = function () {
        var n = $scope.validateCode();
        if (n == 0) {
            if ($('#cu_name').val() != '' && $('#cu_phone').val() != '' && $('#cu_email').val() != '' && $('#cu_subject').val() != '' && $('#cu_comment').val() != '') {
                var emailRule = /^\w+((-\w+)|(\.\w+))*\@[A-Za-z0-9]+((\.|-)[A-Za-z0-9]+)*\.[A-Za-z]+$/;
                if ($('#cu_email').val().search(emailRule) == -1) {
                    alert("請確認您的電子郵件是否正確");
                } else { 
                    $http.post("../../frontendapi/CALLUS", { name: $('#cu_name').val() , phone : $('#cu_phone').val(), email: $('#cu_email').val(), title: $('#cu_subject').val(), content: $('#cu_comment').val() }
                    ).then(function mySucces(response) {
                        alert(response.data);
                        $('#cu_name').val('');
                        $('#cu_phone').val('');
                        $('#cu_email').val('');
                        $('#cu_subject').val('');
                        $('#cu_comment').val('');
                        window.location.reload();
                    });
                }
            }
            else { 
                alert('您的資料不完整，無法送出');
            }
            
        }
    }

    $scope.createCode();
})



﻿var app = angular.module('App', []);
$(document).ready(function () {
    $.ajax({
        type: "GET",
        url: "./frontendapi/BRANCHcountry",
        data: '',
        dataType: "json"
    }).done(function (result) {
        var tdata = result;
        $("#country").append($("<option>請選擇</option>"));
        for (var i = 0; i <= tdata.length - 1 ; i++) {
            $("#country").append($("<option>" + tdata[i].country + "</option>"));
        }
    });
    $('#country').change(function () {
        $("#district").html('');
        $.ajax({
            type: "POST",
            url: "./frontendapi/BRANCHdistrict",
            data: {'country': $("#country").find(":selected").text()},
            dataType: "json"
        }).done(function (result) {
            var tdata = result;
            $("#district").append($("<option>請選擇</option>"));
            for (var i = 0; i <= tdata.length - 1 ; i++) {
                $("#district").append($("<option>" + tdata[i].district + "</option>"));
            }
        });
    });
    $('#district').change(function () {
        $("#branch").html('');
        $.ajax({
            type: "POST",
            url: "./frontendapi/BRANCHbranches",
            data: { 'country': $("#country").find(":selected").text(), 'district': $("#district").find(":selected").text() },
            dataType: "json"
        }).done(function (result) {
            var tdata = result;
            $("#branch").append($("<option>請選擇</option>"));
            for (var i = 0; i <= tdata.length - 1 ; i++) {
                $("#branch").append($("<option value='"+ tdata[i].c_id+"'>" + tdata[i].branches + "</option>"));
            }
        });
    });

    $('#searchBranch').on('click', function () {
        if ($("#branch").find(":selected").val() == '請選擇') {
            alert('請先選擇店家，再送出'); 
        }
        else {
            location.href = './branchesContent?id=' + $("#branch").find(":selected").val();
        }
    });
});
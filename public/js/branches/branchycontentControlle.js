﻿var app = angular.module('App', []);
app.controller('branchycontentControlle', function ($scope, $http) {
        $http.post("../../frontendapi/BranchCantent", { id: c_id }
    ).then(function mySucces(response) {
        $scope.BranchCantent = response.data[0];
         $scope.local = function () {
            $http.post("../../frontendapi/location", { address: $scope.BranchCantent.c_country + $scope.BranchCantent.c_district + $scope.BranchCantent.c_address }
                    ).then(function youSucces(response) {
                        $scope.locationdata = response.data[0];
                        //bing map
                        var map = new Microsoft.Maps.Map(document.getElementById('myMap'), {
                            credentials: 'FUBK8cc2f0N8tp1kcO95~eh8uqqRcuFEv6QT8PIZiAg~AiBpl78m-BA5yMw51aG_yHmhmCtf-Z7mr1eI0hGEW65jIplRAxRKHXe459kZkCsd',//bing key
                            center: new Microsoft.Maps.Location($scope.locationdata.lat, $scope.locationdata.lng),//放location   
                            zoom: 12
                        });
                        var pushpin = new Microsoft.Maps.Pushpin(map.getCenter(), {
                            icon: './images/poi_custom.png',
                            anchor: new Microsoft.Maps.Point(12, 39),
                            title: $scope.BranchCantent.c_name//放店名
                        });
                        map.entities.push(pushpin);
                        //Microsoft.Maps.loadModule('Microsoft.Maps.Directions', function () {
                        //    var directionsManager = new Microsoft.Maps.Directions.DirectionsManager(map);
                        //    directionsManager.setRenderOptions({ itineraryContainer: document.getElementById('printoutPanel') });
                        //    var waypoint2 = new Microsoft.Maps.Directions.Waypoint({ address: $scope.BranchCantent.c_zipcode + $scope.BranchCantent.c_country + $scope.BranchCantent.c_district + $scope.BranchCantent.c_address, location: new Microsoft.Maps.Location(locationdata.lat, locationdata.lng) });//地址 & location
                        //    directionsManager.addWaypoint(waypoint2);
                        //    directionsManager.showInputPanel('directionsInputContainer');
                        //});
                    });
        };
        setTimeout($scope.local, 2000);

    });
    
    $http.post("../../frontendapi/Branchnews", { id: c_id , num: 0 }
    ).then(function mySucces(response) {
        for (var i in response.data) {
            response.data[i].newsDate = new Date(response.data[i].newsDate).toLocaleDateString();
        }
        $scope.Branchnews = response.data;
        var marquee = '';
        for (var i = 0 ; i < $scope.Branchnews.length;i++) {
            marquee += "<li ng-click='NEWSclick(" + $scope.Branchnews[i].newsid + ");'>" + $scope.Branchnews[i].newsTitle + " " + $scope.Branchnews[i].newsDate + "</li>";
        }
        $("#marquee").html(marquee);
        $("#marquee").marquee({ yScroll: "top" });
    });
    var N_Page = 0;
    $scope.NewsUPP = function () {
        if (N_Page > 0) {
            N_Page -= 6;
            $http.post("../../frontendapi/Branchnews", { id: c_id , num: N_Page }
            ).then(function mySucces(response) {
                for (var i in response.data) {
                    response.data[i].newsDate = new Date(response.data[i].newsDate).toLocaleDateString();
                }
                $scope.Branchnews = response.data;
            });
        }
    }
    $scope.NewsDOWNP = function () {
        N_Page += 6;
        $http.post("../../frontendapi/Branchnews", { id: c_id , num: N_Page }
        ).then(function mySucces(response) {
            if (response.data[0]) {
                for (var i in response.data) {
                    response.data[i].newsDate = new Date(response.data[i].newsDate).toLocaleDateString();
                }
                $scope.Branchnews = response.data;
            }
            else {
                N_Page -= 6;
            }
        });
    }

    $http.post("../../frontendapi/SBranchnews", { id: c_id, num: 0 }
    ).then(function mySucces(response) {
        for (var i in response.data) {
            response.data[i].newsDate = new Date(response.data[i].newsDate).toLocaleDateString();
        }
       $scope.SBranchnews = response.data;
   });

    var N_Page_s = 0;
    $scope.SNewsUPP = function () {
        if (N_Page_s > 0) {
            N_Page_s -= 3;
            $http.post("../../frontendapi/SBranchnews", { id: c_id, num: N_Page_s }
            ).then(function mySucces(response) {
                for (var i in response.data) {
                    response.data[i].newsDate = new Date(response.data[i].newsDate).toLocaleDateString();
                }
                $scope.SBranchnews = response.data;
            });
        }
    }
    $scope.SNewsDOWNP = function () {
        N_Page_s += 3;
        $http.post("../../frontendapi/SBranchnews", { id: c_id, num: N_Page_s }
        ).then(function mySucces(response) {
            if (response.data[0]) {
                for (var i in response.data) {
                    response.data[i].newsDate = new Date(response.data[i].newsDate).toLocaleDateString();
                }
                $scope.SBranchnews = response.data;
            }
            else {
                N_Page_s -= 3;
            }
        });
    }
    $scope.NEWSclick = function (n_id) {
        $http.post("../../frontendapi/NEWSsearch", {id:n_id})
        .then(function mySucces(response) {
            if (response.data[0]) {
                response.data[0].articleDate = new Date(response.data[0].articleDate).toLocaleDateString();
            }
            $scope.NEWScontent = response.data;
            $('.newContain').html(($scope.NEWScontent[0].Article));
            $('#news-modal').modal('toggle');
        });
    }
});

/*跑馬燈 https://www.givainc.com/labs/marquee_jquery_plugin.cfm*/
$(document).ready(function () {
    
});
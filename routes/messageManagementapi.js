﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var path = require('path');
var fs = require('fs');

router.get('/loadItem', function (req, res) {
    var db = req.db;
    db.query("SELECT `contentusid` as 'msgId', `name` as 'msgName', `phone` as 'msgPhone', `e-mail` as 'msgEmail', `title` as 'msgTitle', `content` as 'msgContext', `readyet` as 'readyet', `contenttime` as 'msgDate' FROM `o_contentus`;"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.get('/loadnoread', function (req, res) {
    var db = req.db;
    db.query("SELECT COUNT(`contentusid`) as 'contentusReadyetNUM' FROM o_contentus WHERE `readyet` ='0';"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            for (var i in data) {
                
            }
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/updateread', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.o_contentus SET o_contentus.contenttime ='" + data.msgDate + "', o_contentus.readyet = '1'  WHERE o_contentus.contentusid = '" + data.msgId + "';"
, function (err, rows, fields) {
        if (!err) {
            res.end('success');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});


router.post('/deleteItem', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `o_contentus` WHERE `contentusid` ='"+id+"';"
, function (err, rows, fields) {
        if (!err) {
            res.end('success');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});


module.exports = router;
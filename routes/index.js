﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
/* GET home page. */
router.get('/', function (req, res) {
    res.render('index', { title: 'Expressssss' });
});
router.get('/register', function (req, res){
    res.render('register');
})
router.get('/activityArea', function (req, res) {
    var data = req.query;
    if (data.date || data.search) {
        res.render('activityArea', data);
    }
    else {
        res.render('activityArea', {
            date : '' , 
            search : ''
        });
    }
});
router.get('/aboutUs', function (req, res) {
    res.render('aboutUs', { title: 'aboutUs' });
});
router.get('/article', function (req, res) {
    var data = req.query;
    res.render('article', { A_id: data.id ,type :data.type });
});
router.get('/branches', function (req, res) {
    res.render('branches', { title: 'branches' });
});
router.get('/branchesContent', function (req, res) {
    var id = req.query.id;
    console.log(id);
    res.render('branchesContent', { c_id: id });
});
router.get('/checkOut', function (req, res) {
    res.render('checkOut', { title: 'checkOut' });
});
router.get('/contactUs', function (req, res) {
    res.render('contactUs', { title: 'contactUs' });
});
router.get('/knowledgeSharing', function (req, res) {
    var data = req.query;
    if (data.type || data.search) {
        res.render('knowledgeSharing', data);
    }
    else {
        res.render('knowledgeSharing', {
            type : '' , 
            search : ''
        });
    }
});
router.get('/memberArea', function (req, res) {
    res.render('memberArea', { title: 'memberArea' });
});
router.get('/message', function (req, res) {
    res.render('message', { title: 'message' });
});
router.get('/onlineMall', function (req, res) {
    var data = req.query;
    if (data.type|| data.order||data.search) {
        res.render('onlineMall', data);
    }
    else {
        res.render('onlineMall', {
            type : '' , 
            order : 'new',
            search : ''
        });
    }
});
router.get('/productContent', function (req, res) {
    var data = req.query;
    res.render('productContent', data);
});
router.get('/shoppingCart', function (req, res) {
    res.render('shoppingCart', { title: 'shoppingCart' });
});

router.post('/checkfinal', function (req, res) {
    var data = req.body;
    if (data.o_id) {
        res.render('checkfinal', { title: 'checkfinal' , name : data.name , orderid : data.o_id , totalprice : data.totalprice });
    }
    else {
        res.send('error');
    }
});

module.exports = router;
﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var fs = require('fs');

router.post('/news', function (req, res) {
    var db = req.db;
    var c_id = req.body.id;
    var c_type = req.body.type;
    var CW = "";
    if (c_type == 1) { CW = ""; }
    else { CW = "AND o_news.companyids like '%|"+ c_id+"|%'"; }
    var sqls = {
        news: "select o_news.newsid as 'id', o_news.writer as 'Writerid', companylist.name as'Writer' , o_news.date as'DTime', o_news.date as 'Time', o_news.title as 'Title', o_news.clicks as 'View', o_news.statue as 'Publish' , o_news.companyids as 'companyids',o_news.article as 'article' from o_news,companylist WHERE o_news.writer = companylist.companyid "+CW+" ORDER BY o_news.date DESC;"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
        } else {
            var data = results;
            for (var i in data.news) {
                data.news[i].companyids = data.news[i].companyids.substring(1, data.news[i].companyids.length - 1);
                data.news[i].Time = new Date(data.news[i].Time).toLocaleTimeString();
                data.news[i].DateTime = new Date(data.news[i].DTime).toLocaleTimeString();
            }
            res.json(data);
            res.end();
        }
    });
});

router.get('/branch', function (req, res) {
    var db = req.db;
    db.query("SELECT companylist.companyid as 'id' , companylist.name as 'name' from companylist;"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            for (var i in data) {
                data[i].id = data[i].id.toString();
            }
            var idZ = [{ "id": "0", "name": "首頁" }];
            data = idZ.concat(data);
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/insert', function (req, res) {
    var db = req.db;
    if (req.body.Title == '' || req.body.Content == '' || req.body.Publish == '') {
        res.send('datanotenough');
        res.end();
    }
    else {
        var data = req.body;
        var companyids = "";
        if (data.c_type == 1) { companyids = data.Companyid; }
        else { companyids = "|" + data.c_id + "|"; }
        if (data.id == 'NEW') {
            db.query("INSERT INTO `o_news` (`newsid`, `companyids` , `writer` , `title`, `date`, `clicks`, `article`, `statue`) VALUES (NULL, '" + companyids + "' , '"+data.c_id+"' , '" + data.Title + "', NOW(), '0', '" + data.Content + "', '" + data.Publish + "');"
            , function (err, rows, fields) {
                if (!err) {
                    res.end("success insert");
                }
                else {
                    console.log('Error while performing Query.');
                }
            });
        }
        else { 
            db.query("UPDATE yenbao.o_news SET o_news.companyids ='" + companyids + "', o_news.writer = '" + data.c_id + "' , o_news.title ='" + data.Title+ "', o_news.date = '"+data.DateTime+"', o_news.article ='" + data.Content + "', o_news.statue ='" + data.Publish + "' WHERE o_news.newsid = '" + data.id + "';"
            , function (err, rows, fields) {
                if (!err) {
                    res.end("success update");
                }
                else {
                    console.log('Error while performing Query.');
                }
            });
        }
    }
    
});

router.post('/deleteItem', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `o_news` WHERE `newsid`='"+id+"'", function (err, rows, fields) {
        if (!err) {
            console.log('success delete ' + req.body.id);
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
module.exports = router;
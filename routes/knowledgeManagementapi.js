﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var multer = require('multer');
var uploading = multer({ dest: './public/images/uploads/' });
var path = require('path');
var fs = require('fs');

router.get('/knowledge', function (req, res) {
    var db = req.db;
    db.query("select o_knowledge.knowledgeid as 'id', o_knowledge.posttime as'knowDate', o_knowledge.title as 'knowTitle', o_knowledge.picture as 'knowImgSrc', o_knowledge.clicks as 'knowView', o_knowledge.statue as ' knowPublish' from o_knowledge order by o_knowledge.posttime DESC"
        , function (err, rows, fields) {
        if (!err) {
            var data = rows;
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
        
    });
});
router.post('/knowledgeload',  function (req, res) {
    var db = req.db;
    var id = req.body.id;
    var sqls = {
        knowledge: "SELECT o_knowledge.o_k_type as 'knowledgeType', o_knowledge.title as 'knowledgeTitle', o_knowledge.picture as 'knowImgSrc', o_knowledge.picture as 'knowledgeCover', o_knowledge.video as 'knowledgeVideo', o_knowledge.article as 'knowledgeArticle' from o_knowledge where o_knowledge.knowledgeid = '"+id+"'"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
        } else {
            var data = results;
            for (var i in data.knowledge) {
                //data.knowledge[i].knowImgSrc = "./images/knowledge/" + data.knowledge[i].knowImgSrc;
            }
            res.json(data);
            res.end()
        }
    });
    
});

router.post('/pictureload', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    var sqls = {
        pic : "SELECT o_k_pictures.picturesid as 'pictureid', o_k_pictures.picture as 'knowledgePicture' from o_k_pictures, o_knowledge where o_knowledge.knowledgeid = '" + id + "' and o_knowledge.knowledgeid = o_k_pictures.knowledgeid;"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
        } else {
            var data = results;
            for (var j in data.pic) {
                data.pic[j].knowledgeName = data.pic[j].knowledgePicture;
                //data.pic[j].knowledgePicture = "./images/knowledge/" + data.pic[j].knowledgePicture;
            }
            res.json(data);
            res.end()
        }
    });
});
//上傳照片
router.post('/insertpic', uploading.single('uploadCoverImg'), function (req, res) {
    var db = req.db;
    var img = req.file.originalname.substr(-4);
    res.json({'name': req.file.filename + img});
    fs.rename('./public/images/uploads/'+ req.file.filename,'./public/images/knowledge/'+ req.file.filename + img, function (err) {
        if (err) throw err
        console.log('File saved.')
   })
   res.end();
});

router.post('/knowledgeinsert', function (req, res) {
    var db = req.db;
    var data = req.body;
    var sqls = {
        ins : "INSERT INTO `o_knowledge` (`knowledgeid`, `title`, `posttime`, `picture`, `video`, `article`, `clicks`, `statue`, `o_k_type`) " +
        "VALUES (NULL, '" + data.title + "', NOW() , '" + data.images + "', '" + data.video + "', '" + data.content + "', '0', '0', '" + data.type + "');"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
        } else {
            var tdata = results;
            res.json(tdata.ins.insertId);
            res.end("success");
        }
    });
});
//上傳照片區
router.post('/insertpicplace', uploading.single('uploadKnowledgeImg'), function (req, res) {
    var db = req.db;
    var img = req.file.originalname.substr(-4);
    res.json({ 'name': req.file.filename + img });
     fs.rename('./public/images/uploads/'+ req.file.filename,'./public/images/knowledgePIC/' + req.file.filename + img, function (err) {
         if (err) throw err
         console.log('File saved.')
    })
    res.end();
});
//照片區新增
router.post('/imagesinsert', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("INSERT INTO `o_k_pictures` (`picturesid`, `knowledgeid`, `picture`) " +
        "VALUES (NULL, '"+data.id+"', '"+data.images+"');"
, function (err, rows, fields) {
        if (!err) {
            res.end("success");
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
});

router.post('/knowledgeupdate', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.o_knowledge SET o_knowledge.title ='" + data.title + "', o_knowledge.posttime ='" + data.time + "', o_knowledge.picture ='" + data.images + "', o_knowledge.video ='" + data.video + "', o_knowledge.article ='" + data.content + "', o_knowledge.o_k_type ='" + data.type + "' WHERE o_knowledge.knowledgeid ='"+data.id+"';"
, function (err, rows, fields) {
        if (!err) {
            res.json([{"k_id": data.id}]);
            res.end();
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
    
});

router.post('/knowledgeupdatePublish', function (req, res) {
    var db = req.db;
    var data = req.body;
    if (data.knowPublish == 'false') { data.knowPublish = '0'; }
    else { data.knowPublish = '1'; }
    db.query("UPDATE yenbao.o_knowledge SET o_knowledge.posttime ='" + data.knowDate + "', o_knowledge.statue ='" + data.knowPublish + "' WHERE o_knowledge.knowledgeid ='"+data.id+"';"
, function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
});

router.post('/knowledgedelete', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `o_knowledge` WHERE `knowledgeid` = '"+id+"';", function (err, rows, fields) {
        if (!err) {
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
router.post('/knowledgedeletepic', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    console.log(id);
    db.query("DELETE FROM `o_k_pictures` WHERE `picturesid` = '"+id+"';", function (err, rows, fields) {
        if (!err) {
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
//=========類別CRUD============
router.get('/loadtype', function (req, res) {
    var db = req.db;
    db.query("SELECT o_k_type.o_k_type as 'typeNo' , o_k_type.type as 'typeName' FROM o_k_type ;"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/inserttype', function (req, res) {
    var db = req.db;
    var name = req.body.name;
    db.query("INSERT INTO `o_k_type` (`o_k_type`, `type`) VALUES (NULL, '"+name+"');"
, function (err, rows, fields) {
        if (!err) {
            res.end("success");
        }
        else {
            console.log('Error while performing Query.');
        }
    });
    
});


router.post('/updatetype', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.o_k_type SET o_k_type.type ='"+data.typeName+"' WHERE o_k_type.o_k_type = '"+data.typeNo+"';"
, function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});


router.post('/deletetype', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("DELETE FROM `o_k_type` WHERE `o_k_type` = '"+data.id+"';"
, function (err, rows, fields) {
        if (!err) {
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
module.exports = router;
﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var multer = require('multer');
var uploading = multer({ dest: './public/images/uploads/' });
var path = require('path');
var fs = require('fs');

/* GET 分店資料 */
router.get('/branches', function (req, res) {
    var db = req.db;
   db.query("select companylist.companyid as'No',companylist.c_dataid as'dataid', c_data.picture as 'ImgSrc', c_data.addresstype as 'addresstype', c_data.county as 'county', c_data.district as 'district', c_data.zipcode as 'zipcode', companylist.type as 'Type', companylist.name as 'Name', c_data.tel as 'Phone', c_data.address as 'Address', c_data.email as 'Email', c_account.c_account as 'Id', c_account.password as 'Pwd', c_account.article as 'Post', c_account.statue as 'Stop' from companylist, c_data, c_account where companylist.c_dataid = c_data.c_dataid and companylist.c_account = c_account.c_account"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            for (var i in data) {
                data[i].No = "#" + data[i].No;
                data[i].AddressContent = data[i].Address;
                data[i].Address = data[i].county + data[i].district + data[i].Address;
            }
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/insertpic', uploading.single('uploadBranchCoverImg'), function (req, res) {
    var db = req.db;
    var img = req.file.originalname.substr(-4);
    res.json({ 'name': req.file.filename + img });
    fs.rename('./public/images/uploads/' + req.file.filename, './public/images/branch/' + req.file.filename + img, function (err) {
        if (err) throw err
        console.log('File saved.')
    })
    res.end();
});

router.post('/insertItem', function (req, res) {
    var db = req.db;
    var data = req.body;
    if (req.body.Pwd1 != req.body.Pwd2) {
        res.send('Pwderror');
        res.end();
    }
    else if (req.body.No == '' || req.body.Name == '' || req.body.Phone == '' || req.body.Address == '' || req.body.Email == '' || req.body.Id == '' || req.body.Pwd1 == '' || req.body.Pwd2 == '' ||req.body.images =='' || req.body.zipcode == '') {
        res.send('dataerror');
        res.end();
    }
    else {
        //資料都已完整
        if (data.No == '#NEW') {
            //insert
            db.query("select c_account.c_account as 'ngid' from c_account where c_account.c_account = '" + data.Id + "';" , 
                function (err, rows, fields) {
                if (!err) {
                    if (rows[0]) {
                        res.send('IDrepeat');
                        res.end();
                    }
                    else {
                        //撈出最大的No
                        var N_id;
                        db.query("select c_data.c_dataid as 'highid' from c_data order by c_data.c_dataid DESC LIMIT 1;", 
                              function (err, rows, fields) {
                            if (!err) {
                                if (rows[0]) {
                                    N_id = rows[0].highid + 1;
                                } else { N_id = 0; }
                                var type = parseInt(data.Type) + 1;
                                //新增語法
                                var sqls = {
                                    n_c_data: "INSERT INTO `c_data` (`c_dataid`, `tel` , `addresstype` , `county`,`district`, `zipcode`, `address`, `email`, `picture`) VALUES('" + N_id + "', '" + data.Phone + "','" + data.addresstype + "','" + data.county + "','" + data.district + "','" + data.zipcode + "', '" + data.Address + "', '" + data.Email + "', '" + data.images + "');",
                                    n_c_id : "INSERT INTO `c_account` (`c_account`, `password`, `statue`, `article`)  VALUES('" + data.Id + "', '" + data.Pwd1 + "', '" + data.Stop + "', '" + data.Post + "');",
                                    n_c : "INSERT INTO `companylist` (`companyid`, `type`, `name`, `c_dataid`, `c_account`)  VALUES(NULL, '" + type + "', '" + data.Name + "', '" + N_id + "', '" + data.Id + "');"
                                };
                                async.map(sqls, function (item, callback) {
                                    db.query(item, function (err, results) {
                                        callback(err, results);
                                    });
                                }, function (err, results) {
                                    if (err) {
                                        res.send(err);
                                        console.log('Error while performing Query.');
                                    } else {
                                        res.send("success insert");
                                        console.log("success insert");
                                        res.end();
                                    }
                                });
                                 //這邊以上是新增語法
                            }
                        });
                    }
                }
            
            });
        }
        //update
        else {
            var chsqls = {
                chrepeat: "select c_account.c_account as 'ngid' from c_account where c_account.c_account = '" + data.Id + "';",
                chid : "select companylist.c_account as 'id' from companylist where companylist.companyid = '" + data.No.substring(1 , data.No.length) + "';"
            };
            async.map(chsqls, function (item, callback) {
                db.query(item, function (err, results) {
                    callback(err, results);
                });
            }, function (err, results) {
                if (err) {
                    res.send(err);
                    console.log('Error while performing Query.');
                } else {
                    console.log(results.chrepeat[0].ngid);
                    if (results.chrepeat[0] && results.chrepeat[0].ngid != results.chid[0].id) {
                        res.send('IDrepeat');
                        res.end();
                    }
                    else {
                        var No = data.No.substring(1 , data.No.length);
                        var type = parseInt(data.Type) + 1;
                        db.query("update companylist ,c_data, c_account " +
                             "set companylist.name = '" + data.Name + "' , companylist.type = '" + type + "', c_data.tel = '" + data.Phone + "', c_data.addresstype = '" + data.addresstype + "', c_data.county = '" + data.county + "', c_data.district = '" + data.district + "', c_data.zipcode = '" + data.zipcode + "', c_data.address = '" + data.Address + "', c_data.email = '" + data.Email + "',c_account.c_account = '" + data.Id + "',companylist.c_account = '" + data.Id + "', c_account.password = '" + data.Pwd1 + "', c_account.statue = '" + data.Stop + "', c_account.article = '" + data.Post + "' , c_data.picture = '" + data.images + "' " +
                          " WHERE companylist.companyid = '" + No + "' && companylist.c_dataid = c_data.c_dataid && companylist.c_account = c_account.c_account;"
                            , function (err, rows, fields) {
                            if (!err) {
                                res.send("success update");
                                res.end();
                                console.log("success update");
                            }
                            else {
                                console.log('Error while performing Query.');
                            }
                        });
                    }
                }
            });
        }
        
        
    }
    
});
router.post('/deleteItem', function (req, res) {
    var db = req.db;
    var data = req.body;
    var sqls = {
        companylist: "DELETE FROM `companylist` WHERE `c_dataid` = '" + data.id + "';",
        c_account : "DELETE FROM `c_account` WHERE `c_account` = '" + data.account + "';",
        c_data : "DELETE FROM `c_data` WHERE `c_dataid` = '" + data.id + "';"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
            console.log('Error while performing Query.');
            res.end();
        } else {
            res.send("success delete");
            console.log("success delete");
            res.end();
        }
    });
});
module.exports = router;
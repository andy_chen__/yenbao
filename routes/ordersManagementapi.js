﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var path = require('path');
var fs = require('fs');

//定期更新訂單
router.get('/LoadOrdersNoPay', function (req, res) {
    var db = req.db;
    db.query("SELECT COUNT(`ordersID`) as 'NUM' FROM m_carsend WHERE `statue` ='0';"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

//載入訂單

router.get('/loadItem', function (req, res) {
    var db = req.db;
    db.query("SELECT * FROM `m_carsend`;"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            for (var i in data) {
                data[i].ODtime = data[i].ordertime;
                data[i].ordertime = new Date(data[i].ODtime).toLocaleTimeString();
                data[i].OTtime = data[i].outtime;
                if (data[i].OTtime == null) { data[i].outdate = '未出貨'; data[i].outtime = '未出貨'; }
                else {
                data[i].outtime = new Date(data[i].OTtime).toLocaleTimeString();
                }
            }
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
//修改訂單 收件資訊
router.post('/updateItem', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.m_carsend SET m_carsend.name = '"+data.name+"', m_carsend.zip = '"+data.zip+"', m_carsend.address = '"+data.address+"' WHERE m_carsend.ordersID = '"+data.ordersID+"';"
, function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
//刪除訂單
router.post('/deleteItem', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `m_carsend` WHERE `ordersID` ='" + id + "';"
, function (err, rows, fields) {
        if (!err) {
            res.end('success');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
//載入訂單內容
router.post('/loadcontent', function (req, res) {
    var db = req.db;
    var id = req.body.o_id;
    db.query("SELECT m_car.carid as 'c_id' , m_car.productid as 'p_id' , p_data.name as 'p_name' ,m_car.amount as 'amount' , m_car.prescription as 'prescription' ,m_car.hypermyopia as 'hypermyopia' , m_car.rightOD as 'rightOD' ,m_car.leftOD as 'leftOD' , m_car.PD as 'PD' , m_car.specialprocess as 'specialprocess' , m_car.remark as 'remark', m_car.totalprice as 'totalprice' , m_car.dividend as 'dividend' FROM m_car,productlist,p_data WHERE m_car.productid = productlist.productid AND productlist.p_dataid = p_data.p_dataid AND m_car.ordersID = '"+id+"';"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            for (i in data) {
                if (data[i].specialprocess != null) {
                    data[i].specialprocess = data[i].specialprocess.substring(1, data[i].specialprocess.length - 1);
                }
            }
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/deletecontent', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `m_car` WHERE `carid` ='" + id + "';"
, function (err, rows, fields) {
        if (!err) {
            res.end('success');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/searchspecial', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT p_specialprocess.name , p_specialprocess.amount FROM p_specialprocess WHERE p_specialprocess.p_processid = '"+data.id+"';"
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                var tdata = rows[0];
                res.json({i:data.i,content:tdata.name+'('+tdata.amount+') '});
                res.end();
            } else { res.end();}
            
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
//出貨
router.post('/outorders', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    var msqls = {
        update : "UPDATE yenbao.m_carsend SET m_carsend.statue = '1', m_carsend.outtime = NOW() WHERE m_carsend.ordersID = '" + id + "';",
        loaddate:"SELECT * FROM `m_car` WHERE m_car.ordersID = '" + id + "';"
    };
    async.map(msqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            console.log('Error while performing Query.');
            res.send(err);
            res.end();
        } else {
            var tdata = results.loaddate;
            res.json(tdata);
            res.end('success');
        }
    });
});

//寫入購物車內文至消費紀錄
router.post('/outcar', function (req, res) {
    var db = req.db;
    var data = req.body;
    var hypermyopia, rightOD, leftOD, PD, specialprocess, remark;
    if (data.hypermyopia == null || data.hypermyopia == 0) { hypermyopia = null; } else { hypermyopia = "'" + data.hypermyopia + "'"; }
    if (data.rightOD == null || data.rightOD == '') { rightOD = null; } else { rightOD = "'" + data.rightOD + "'"; }
    if (data.leftOD == null || data.leftOD == '') { leftOD = null; } else { leftOD = "'" + data.leftOD + "'"; }
    if (data.PD == null || data.PD == '') { PD = null; } else { PD = "'" + data.PD + "'"; }
    if (data.specialprocess == null || data.specialprocess == '') { specialprocess = null; } else { specialprocess = "'" + data.specialprocess + "'"; }
    if (data.remark == null || data.remark == '') { remark = null; } else { remark = "'" + data.remark + "'"; }
    var msqls = {
        updateamount : "UPDATE yenbao.productlist SET productlist.stock = productlist.stock - "+ data.amount+" WHERE productlist.productid = '" + data.productid + "';",
        insertdeal : "INSERT INTO `m_deal` (`dealid`, `memberid`, `companyid`, `productid`, `date`, `amount`, `returnornot`, `hypermyopia`, `rightOD`, `leftOD`, `PD`, `specialprocess`, `remark`, `totalprice`, `dividend`) " +
        " VALUES (NULL, '"+data.memberid+"', '-1', '"+data.productid+"', NOW(), '" + data.amount + "', '0', " + hypermyopia + ", " + rightOD + ", " + leftOD + ", " + PD + ", " + specialprocess + ", " + remark + ", '10', '1');",
        memberdividend :"UPDATE yenbao.memberlist SET memberlist.dividend = memberlist.dividend + " + data.dividend + " WHERE memberlist.memberid = '" + data.memberid + "';"
    };
    async.map(msqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            console.log('Error while performing Query.');
            res.send(err);
            res.end();
        } else {
            res.json({i:data.i});
            res.end('success');
        }
    });
});

module.exports = router;
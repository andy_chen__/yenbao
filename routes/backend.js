﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
/* GET backend home page. */
router.post('/', function (req, res) {
    var db = req.db;
    var account = req.body.id.substring(1 , req.body.id.length);
    var pwd = req.body.pwd;
    db.query("SELECT companylist.companyid as'c_id', companylist.name as 'c_name', companylist.type as 'c_type', c_account.article as 'c_article', c_account.c_account as 'c_account', c_account.password as 'c_password' "
    +"FROM c_account,companylist WHERE c_account.c_account = companylist.c_account AND c_account.c_account = '"+ account+"' AND c_account.password = '"+pwd+"' AND c_account.statue = 0;"
    , function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                var tdata = rows[0];
                res.render('backend',tdata);
                res.end();
            }
            else {
                //res.render('error', {error:'Error: Not Found'});
                res.send('登入失敗 請記好帳號密碼 累了嗎??');
                res.end();
            }
        }
        else {
            console.log('Error while performing Query.'); res.end();

        }
    });
});

router.post('/message', function (req, res) {
    var db = req.db;
    var data = req.body;
    var sqls = {
        QAmessage: "SELECT p_qna.p_qnaid as 'QAid' , p_qna.name as 'QAname' , p_qna.date as 'QAdate' , p_qna.productid as 'QAp_id', p_data.name as 'QAp_name', p_data.price as 'QAp_price', p_type.type as 'QAp_type' , p_data.name as 'QAp_name' , p_qna.reply as 'QAreply' FROM p_qna,productlist,p_data,p_type WHERE p_qna.productid = productlist.productid AND productlist.p_typeid = p_type.p_typeid AND productlist.p_dataid = p_data.p_dataid ORDER by p_qna.date DESC LIMIT 7;",
        tic : "SELECT COUNT(p_qna.p_qnaid) as 'NUM' FROM p_qna WHERE p_qna.reply ='0';"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
        } else {
            var tdata = results;
            res.json(tdata);
            res.end();
        }
    });
});
router.post('/BIGERmessage', function (req, res) {
    var db = req.db;
    var num = req.body.num;
    var sqls = {
        QAmessage: "SELECT p_qna.p_qnaid as 'QAid' , p_qna.name as 'QAname' , p_qna.date as 'QAdate' , p_qna.productid as 'QAp_id', p_data.name as 'QAp_name', p_data.price as 'QAp_price', p_type.type as 'QAp_type' , p_data.name as 'QAp_name' , p_qna.reply as 'QAreply' FROM p_qna,productlist,p_data,p_type WHERE p_qna.productid = productlist.productid AND productlist.p_typeid = p_type.p_typeid AND productlist.p_dataid = p_data.p_dataid ORDER by p_qna.date DESC LIMIT "+num+";"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
        } else {
            var tdata = results;
            res.json(tdata);
            res.end();
        }
    });
});
module.exports = router;
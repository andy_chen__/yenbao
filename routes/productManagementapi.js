﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var multer = require('multer');
var uploading = multer({ dest: './public/images/uploads/' });
var path = require('path');
var fs = require('fs');

router.get('/product', function (req, res) {
    var db = req.db;
    var sqls = {
        types: "select p_type.type as 'productType', p_type.p_typeid as 'productTypeId' from p_type;",
        products: "SELECT productlist.productid as 'productId', p_data.name as 'productName', p_data.picture as 'productImg', productlist.p_typeid as 'productType',p_type.type as 'productTypeName',p_data.price as 'productPrice', productlist.stock as 'productStock', productlist.statue as 'productPublish', p_data.dividend as 'productDividend', productlist.prescription as 'productPrescript', productlist.specialprocess as 'productSpecial', p_data.material as 'productMaterial',p_data.anthmaterial as 'productAnthMaterial', p_data.content as 'productContent', productlist.click as 'productclick' FROM productlist, p_data,p_type WHERE productlist.p_dataid = p_data.p_dataid AND productlist.p_typeid = p_type.p_typeid;"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.end(err);
        } else {
            var data = results;
            var j = 1;
            var type = [{'productType':'所有類別','productTypeId':0}];
            for (var i in data.types) {
                type[j] = data.types[i];
                j++;
            }
            data.types = type;
            for (var i in data.products) {
                data.products[i].productId = "P" + data.products[i].productId;
                data.products[i].productSpecial = data.products[i].productSpecial.substring(1, data.products[i].productSpecial.length - 1);
            }
            res.json(data);
        }
    });
});

router.post('/productinsert', function (req, res) {
    var db = req.db;
    var data = req.body;
    var P_id = 0;
    db.query("SELECT p_data.p_dataid as 'p_id' From p_data Order by p_data.p_dataid DESC LIMIT 1;", 
       function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                P_id = rows[0].p_id + 1;
            } else { P_id = 0; }
            var msqls = {
                m1 : "INSERT INTO `productlist` (`productid`, `p_typeid`, `p_dataid`, `stock`, `statue`, `prescription`, `specialprocess`,`click`) "+
            "VALUES(NULL, '" + data.typeid + "', '" + P_id + "', '" + data.stock + "', '0', '"+data.prescription+"', '"+data.specialprocess+"', '0');",
                m2 : "INSERT INTO `p_data` (`p_dataid`, `price`, `name`, `picture`, `content`, `material`, `anthmaterial`, `dividend`) "+
                     "VALUES('" + P_id + "', '" + data.price + "', '" + data.name + "', '" + data.picture + "', '" + data.content + "', '" + data.material + "', '" + data.anthmaterial + "', '" + data.dividend + "');"
            };
            async.map(msqls, function (item, callback) {
                db.query(item, function (err, results) {
                    callback(err, results);
                });
            }, function (err, results) {
                if (err) {
                    console.log('Error while performing Query.');
                    res.send(err);
                    res.end();
                } else {
                    var tdata = results;
                    res.json(tdata.m1.insertId);
                    res.end('success');
                }
            });
        }
    });
});

router.post('/productupdate', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.productlist , p_data SET productlist.p_typeid = '" + data.typeid + "' , productlist.stock = '" + data.stock + "' , productlist.prescription = '" + data.prescription + "' , productlist.specialprocess = '" + data.specialprocess + "', "+
    "p_data.price = '" + data.price + "' , p_data.name = '" + data.name + "', p_data.picture = '" + data.picture + "' , p_data.content = '" + data.content + "' , p_data.material = '" + data.material + "', p_data.anthmaterial = '" + data.anthmaterial + "' , p_data.dividend = '" + data.dividend + "' "+
    "WHERE p_data.p_dataid = productlist.p_dataid AND productlist.productid = '"+data.p_id+"';"
, function (err, rows, fields) {
        if (!err) {
            res.json([]);
            res.end("success");
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/productupdatestatue', function (req, res) {
    var db = req.db;
    var data = req.body;
    var p_id = data.productId;
    p_id = p_id.slice(1, p_id.length);
    if (data.productPublish == 'false') { data.productPublish = '0'; }
    else { data.productPublish = '1'; }
    db.query("UPDATE yenbao.productlist SET productlist.statue ='" + data.productPublish + "' WHERE productlist.productid = '" + p_id + "';"
, function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.get('/productType', function (req, res) {
    var db = req.db;
    db.query("select p_type.type as 'productType', p_type.p_typeid as 'productTypeId' from p_type;"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
    
});
router.post('/insertpic', uploading.single('uploadProductImg'), function (req, res) {
    var db = req.db;
    var img = req.file.originalname.substr(-4);
    res.json({ 'name': req.file.filename + img });
    fs.rename('./public/images/uploads/' + req.file.filename, './public/images/product/' + req.file.filename + img, function (err) {
        if (err) throw err
        console.log('File saved.')
    })
    res.end();
});

router.get('/work', function (req, res) {
    var db = req.db;
    db.query("SELECT `p_processid` as 'workNo', `name` as 'workName', `amount` as 'workAmount' FROM `p_specialprocess` WHERE `statue` = '0'"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            for (var i in data) {
                data[i].workNo = new String(data[i].workNo);
                data[i].workName = data[i].workName + '('+ data[i].workAmount + ')';
              }
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

//上傳照片區
router.post('/insertpicplace', uploading.single('uploadProductImgplace'), function (req, res) {
    var db = req.db;
    var img = req.file.originalname.substr(-4);
    res.json({ 'name': req.file.filename + img });
    fs.rename('./public/images/uploads/' + req.file.filename, './public/images/productPIC/' + req.file.filename + img, function (err) {
        if (err) throw err
        console.log('File saved.')
    })
    res.end();
});
//照片區新增
router.post('/imagesinsert', function (req, res) {
    var db = req.db;
    var data = req.body;
    var p_id = data.id;
    p_id = p_id.slice(1, p_id.length);
    db.query("INSERT INTO `p_photo` (`p_photoid`, `name`, `productid`) " +
        "VALUES (NULL, '" + data.images + "', '" + p_id + "');"
, function (err, rows, fields) {
        if (!err) {
            res.end("success");
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
});

router.post('/pictureload', function (req, res) {
    var db = req.db;
    var p_id = req.body.ProductId;
    p_id = p_id.slice(1, p_id.length);
        db.query("SELECT p_photo.p_photoid as 'p_photoid', p_photo.name as 'ProductPics' from p_photo, productlist where productlist.productid = '"+ p_id+"' and productlist.productid = p_photo.productid;"
    , function (err, rows, fields) {
            if (!err) {
            var data = rows;
                res.json(data);
                res.end();
            }
            else {
                console.log('Error while performing Query.');
            }
        });
});
router.post('/picturepicDelete', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `p_photo` WHERE `p_photoid` = '" + id + "';", function (err, rows, fields) {
        if (!err) {
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

//Q&A
router.post('/qaload', function (req, res) {
    var db = req.db;
    var p_id = req.body.ProductId;
    p_id = p_id.slice(1, p_id.length);
    var msqls = {
        m1 : "SELECT p_qna.p_qnaid as 'QAid' , p_qna.memberid as 'memberid' , p_qna.name as 'QAname' , memberlist.email as 'QAemail' , memberlist.tel as 'QAphone' , p_qna.date as 'date' , p_qna.question as 'QAquestion', p_qna.request as 'QArequest', p_qna.reply as 'QAreply' FROM p_qna,memberlist WHERE p_qna.memberid = memberlist.memberid AND p_qna.productid = '"+ p_id+"';",
        m2 : "SELECT p_qna.p_qnaid as 'QAid' , p_qna.name as 'QAname' , p_qna.email as 'QAemail' , p_qna.tel as 'QAphone' , p_qna.date as 'date', p_qna.question as 'QAquestion' , p_qna.request as 'QArequest', p_qna.reply as 'QAreply' FROM p_qna WHERE p_qna.memberid IS null AND p_qna.productid = '"+p_id+"';"
    };
    async.map(msqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            console.log('Error while performing Query.');
            res.send(err);
            res.end();
        } else {
            var tdata = results
            for (var i in tdata.m1) {
                tdata.m1[i].QAdate = new Date(tdata.m1[i].date).toLocaleTimeString();
            }
            for (var i in tdata.m2) {
                tdata.m2[i].QAdate = new Date(tdata.m2[i].date).toLocaleTimeString();
                tdata.m2[i].memberid = '訪客';
            }
            //陣列合併
            var m1_lenght = tdata.m1.length;
            var m2_lenght = tdata.m2.length;
            for (var i = m1_lenght; i <= (m1_lenght + m2_lenght) - 1; i++) {
                tdata.m1[i] = tdata.m2[i - (m1_lenght)];
            }
            //排序
            var len = tdata.m1.length;
            for (var i=0; i < len - 1; i++) {
                for (var k = i + 1; k < len; k++) {
                    var fdate = (tdata.m1[i].QAdate).replace("-", "/");
                    var sdate = (tdata.m1[k].QAdate).replace("-", "/");
                    if (Date.parse(fdate).valueOf() < Date.parse(sdate).valueOf()) {
                        var haham1 = tdata.m1[i];
                        tdata.m1[i] = tdata.m1[k];
                        tdata.m1[k] = haham1;
                    }
                }
            }
            res.json(tdata.m1);
            res.end();
        }
    });
});


router.post('/qarequest', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.p_qna SET p_qna.request = '"+data.qa_request+"', p_qna.reply = '1', p_qna.replytime = NOW() WHERE p_qna.p_qnaid = '"+data.qa_id+"';"
, function (err, rows, fields) {
        if (!err) {
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
router.post('/qarequestdelete', function (req, res) {
    var db = req.db;
    var data = req.body;
     db.query("UPDATE yenbao.p_qna SET p_qna.request = null, p_qna.reply = '0', p_qna.replytime = null WHERE p_qna.p_qnaid = '"+data.qa_id+"';"
, function (err, rows, fields) {
        if (!err) {
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});


router.post('/qadelete', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `p_qna` WHERE `p_qnaid` ='" + id + "'", function (err, rows, fields) {
        if (!err) {
            console.log('success delete ' + id);
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
//A&Q

//特殊加工
router.get('/loadwork', function (req, res) {
    var db = req.db;
    db.query("SELECT `p_processid` as 'workNo', `name` as 'workName' , `amount` as 'workAmount'  ,`statue` as 'workStatue' FROM `p_specialprocess`"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/insertwork', function (req, res) {
    var db = req.db;
    var name = req.body.name;
    db.query("INSERT INTO `p_specialprocess` (`p_processid`, `name`,`amount`, `statue`) VALUES (NULL, '"+name+"','0','0');"
, function (err, rows, fields) {
        if (!err) {
            res.end("success");
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/updatework', function (req, res) {
    var db = req.db;
    var data = req.body;
    if (data.workStatue == 'false') { data.workStatue = '0'; }
    else { data.workStatue = '1'; }
    db.query("UPDATE yenbao.p_specialprocess SET p_specialprocess.name ='"+data.workName+"' , p_specialprocess.amount ='" + data.workAmount + "' , p_specialprocess.statue ='" + data.workStatue + "' WHERE p_specialprocess.p_processid = '"+data.workNo+"';"
, function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
//工加殊特

//產品父類別
router.get('/loadftype', function (req, res) {
    var db = req.db;
    db.query("SELECT `p_ftypeid` as 'ftypeNo', `ftype` as 'ftypeName' ,`statue` as 'ftypeStatue' FROM `p_ftype`"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/insertftype', function (req, res) {
    var db = req.db;
    var name = req.body.name;
    db.query("INSERT INTO `p_ftype` (`p_ftypeid`, `ftype`, `statue`) VALUES (NULL, '" + name + "','0');"
, function (err, rows, fields) {
        if (!err) {
            res.end("success");
        }
        else {
            console.log('Error while performing Query.');
        }
    });
    
});

router.post('/updateftype', function (req, res) {
    var db = req.db;
    var data = req.body;
    if (data.ftypeStatue == 'false') { data.ftypeStatue = '0'; }
    else { data.ftypeStatue = '1'; }
    db.query("UPDATE yenbao.p_ftype SET p_ftype.ftype ='" + data.ftypeName + "', p_ftype.statue ='" + data.ftypeStatue + "' WHERE p_ftype.p_ftypeid = '" + data.ftypeNo + "';"
, function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

//別類父品產

//產品子類別
router.post('/loadstype', function (req, res) {
    var db = req.db;
    var ftypeNo = req.body.ftypeNo;
    db.query("SELECT p_type.p_typeid as 'stypeNo', p_type.type as 'stypeName' , p_type.statue as 'stypeStatue' FROM p_type WHERE p_type.p_ftypeid = '"+ ftypeNo+"';"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/insertstype', function (req, res) {
    var db = req.db;
    var name = req.body.name;
    var ftypeNo = req.body.ftypeNo;
    db.query("INSERT INTO `p_type` (`p_typeid`, `p_ftypeid`, `type` , `statue`) VALUES (NULL,'" + ftypeNo + "', '" + name + "','0');"
, function (err, rows, fields) {
        if (!err) {
            res.end("success");
        }
        else {
            console.log('Error while performing Query.');
        }
    }); 
});

router.post('/updatestype', function (req, res) {
    var db = req.db;
    var data = req.body;
    if (data.stypeStatue == 'false') { data.stypeStatue = '0'; }
    else { data.stypeStatue = '1'; }
    db.query("UPDATE yenbao.p_type SET p_type.type ='" + data.stypeName + "', p_type.statue ='" + data.stypeStatue + "' WHERE p_type.p_typeid = '" + data.stypeNo + "';"
, function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
//別類子品產
//消費紀錄
router.post('/productpurchase', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    id = id.slice(1, id.length);
    var sqls = {
        levels: "SELECT m_level.m_levelid as 'MemberLevelId' , m_level.level as 'MemberLevel' FROM m_level;",
        purchase: "SELECT m_deal.dealid as 'PurchaseId',m_deal.companyid as 'c_id', m_deal.memberid as 'MemberId', memberlist.name as 'MemberName', memberlist.m_levelid as 'MemberLevel', m_deal.date as 'PurchaseDate', m_deal.productid as 'ProductId', p_data.name as 'ProductName', productlist.p_typeid as 'ProductType', m_deal.amount as 'ProductNum', m_deal.totalprice as 'ProductCost', m_deal.dividend as 'Dividend', m_deal.returnornot as 'Return',m_deal.hypermyopia as 'hypermyopia' FROM m_deal,memberlist,p_data,productlist WHERE m_deal.memberid = memberlist.memberid AND m_deal.productid = productlist.productid AND productlist.p_dataid = p_data.p_dataid  AND m_deal.productid = '" + id + "';" ,
        product: "select p_type.type as 'ProductType', p_type.p_typeid as 'ProductTypeId' from p_type;",
        amount : "SELECT SUM(m_deal.amount) as 'amount' FROM m_deal WHERE m_deal.returnornot = '0' AND m_deal.productid ='" + id + "';",
        company : "SELECT companylist.companyid as 'StoreId' , companylist.name as 'Store' FROM companylist;"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.end(err);
        } else {
            var tdata = results;
            var j = 1;
            var levels = [{ 'MemberLevel': '所有等級', 'MemberLevelId': 0 }];
            for (var i in tdata.levels) {
                levels[j] = tdata.levels[i];
                j++;
            }
            tdata.levels = levels;
            var c = 1;
            var product = [{ 'ProductType': '所有類別', 'ProductTypeId': 0 }];
            for (var i in tdata.product) {
                product[c] = tdata.product[i];
                c++;
            }
            tdata.product = product;
            var c = 2;
            var company = [{ 'Store': '所有店家', 'StoreId': 0 }, { 'Store': '網路購物', 'StoreId': -1 }];
            for (var i in tdata.company) {
                company[c] = tdata.company[i];
                c++;
            }
            tdata.company = company;
            for (var i in tdata.purchase) {
                tdata.purchase[i].PurchaseDate = new Date(tdata.purchase[i].PurchaseDate).toLocaleString();
            }
            if (tdata.amount[0].amount == null) { tdata.amount[0].amount = 0; }
            res.json(tdata);
            res.end();
        }
    });
});
module.exports = router;
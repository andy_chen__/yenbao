﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var multer = require('multer');
var uploading = multer({ dest: './public/images/uploads/' });
var path = require('path');
var fs = require('fs');
//公司個黨
router.post('/companydataload', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT companylist.companyid as'c_id', companylist.name as 'c_name', c_account.c_account as 'c_account', c_account.password as 'c_password' , c_data.picture as 'c_picture' , c_data.county as 'c_county' , c_data.district as 'c_district', c_data.address as 'c_address', c_data.tel as 'c_phone', c_data.email as 'c_email', c_data.opentime as 'c_time' " 
    + "FROM c_account,companylist,c_data WHERE companylist.c_dataid = c_data.c_dataid AND c_account.c_account = companylist.c_account AND companylist.companyid ='"+data.id+"';"
, function (err, rows, fields) {
        if (!err) {
            var tdata = rows[0];
            res.json(tdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
router.post('/companydataupdate', function (req, res) {
    var db = req.db;
    var data = req.body;
    var chsqls = {
        chrepeat: "select c_account.c_account as 'ngid' from c_account where c_account.c_account = '" + data.c_account + "';",
        chid : "select companylist.c_account as 'id' from companylist where companylist.companyid = '" + data.c_id + "';"
    };
    async.map(chsqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
            console.log('Error while performing Query.');
        } else {
            console.log(results.chrepeat[0].ngid);
            if (results.chrepeat[0] && results.chrepeat[0].ngid != results.chid[0].id) {
                res.send('IDrepeat');
                res.end();
            }
            else {
            //update
                db.query("UPDATE companylist,c_account,c_data SET c_data.tel = '"+data.c_phone+"', c_data.email = '"+data.c_email+"', c_data.opentime = '"+data.c_time+"', c_account.c_account = '" + data.c_account + "', companylist.c_account = '"+data.c_account+"', c_account.password = '"+data.c_pwd+"' WHERE companylist.c_dataid = c_data.c_dataid AND companylist.c_account = c_account.c_account AND companylist.companyid = '"+data.c_id+"';"
                    , function (err, rows, fields) {
                    if (!err) {
                        res.end("success");
                    }
                    else {
                        console.log('Error while performing Query.');
                    }
                });
            }
        }
    });
});

router.post('/insertpic', uploading.single('uploadCoverImg'), function (req, res) {
    var db = req.db;
    var img = req.file.originalname.substr(-4);
    res.json({ 'name': req.file.filename + img });
    fs.rename('./public/images/uploads/' + req.file.filename, './public/images/branch/' + req.file.filename + img, function (err) {
        if (err) throw err
        console.log('File saved.')
    })
    res.end();
});

router.post('/companypicturesave', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE companylist,c_data SET c_data.picture = '"+data.c_picture+"' WHERE companylist.c_dataid = c_data.c_dataid AND companylist.companyid = '"+data.c_id+"';"
                    , function (err, rows, fields) {
        if (!err) {
            res.end("success");
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

//年度記事
router.get('/aboutusload', function (req, res) {
    var db = req.db;
    db.query("SELECT o_aboutus.aboutusid as 'historyid',o_aboutus.year as 'historyYear',o_aboutus.title as 'historyText' FROM o_aboutus ORDER BY o_aboutus.year ASC;"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
           
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/insertaboutus', function (req, res) {
    var db = req.db;
    var year = req.body.year;
    var title = req.body.title;
    db.query("INSERT INTO `o_aboutus` (`aboutusid`, `year`, `title`) VALUES (NULL, '" + year + "','"+title+"');"
, function (err, rows, fields) {
        if (!err) {
            res.end("success");
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/updateaboutus', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.o_aboutus SET o_aboutus.year ='" + data.historyYear + "', o_aboutus.title ='" + data.historyText + "' WHERE o_aboutus.aboutusid = '" + data.historyid + "';"
, function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end("success");
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/deleteboutus', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `o_aboutus` WHERE `aboutusid`='" + id + "'", function (err, rows, fields) {
        if (!err) {
            console.log('success delete ' + req.body.id);
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

//匯款方法
router.get('/payload', function (req, res) {
    var db = req.db;
    db.query("SELECT howtopay.howtopayid , howtopay.content FROM howtopay ORDER BY howtopay.howtopayid DESC LIMIT 1;"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
router.post('/payinsert', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("INSERT INTO `howtopay` (`howtopayid`, `datetime`, `content`) VALUES (NULL, NOW(), '"+data.data+"');"
, function (err, rows, fields) {
        if (!err) {
            res.json('success');
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
module.exports = router;
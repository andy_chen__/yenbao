﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var multer = require('multer');
var uploading = multer({ dest: './public/images/uploads/' });
var path = require('path');
var fs = require('fs');

router.get('/activityload', function (req, res) {
    var db = req.db;
    db.query('select o_activity.activityid as "activityID" ,  o_activity.posttime as "activityDate" , o_activity.title as "activityTitle", o_activity.picture as "activityImgSrc", o_activity.video as "activityVideo", o_activity.article as "activityArticle" , o_activity.clicks as "activityView", o_activity.statue as "activityPublish" from o_activity order by o_activity.posttime DESC'
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            for (var i in data) {
                //data[i].activityImgSrc = "./images/activity/" + data[i].activityImgSrc;
                //data[i].activityDate = new Date(data[i].activityDate).toLocaleDateString();
            }
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/insertpic', uploading.single('A_uploadCoverImg'), function (req, res) {
    var db = req.db;
    var img = req.file.originalname.substr(-4);
    res.json({ 'name': req.file.filename + img });
    fs.rename('./public/images/uploads/' + req.file.filename, './public/images/activity/' + req.file.filename + img, function (err) {
        if (err) throw err
        console.log('File saved.')
    })
    res.end();
});

router.post('/activityinsert', function (req, res) {
    var db = req.db;
    var data = req.body;
    var sqls = {
        ins : "INSERT INTO `o_activity` (`activityid`, `title`, `posttime`, `picture`, `video`, `article`, `clicks`, `statue`) " +
        "VALUES(NULL, '" + data.title + "', NOW(), '" + data.images + "', '" + data.video + "', '" + data.content + "', '0', '0');"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
        } else {
            var tdata = results;
            res.json(tdata.ins.insertId);
            res.end("success");
        }
    });
});

router.post('/insertpicplace', uploading.single('uploadActivityImg'), function (req, res) {
    var db = req.db;
    var img = req.file.originalname.substr(-4);
    res.json({ 'name': req.file.filename + img });
    fs.rename('./public/images/uploads/' + req.file.filename, './public/images/activityPIC/' + req.file.filename + img, function (err) {
        if (err) throw err
        console.log('File saved.')
    })
    res.end();
});

router.post('/imagesinsert', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("INSERT INTO `o_a_pictures` (`o_a_pictureid`, `activityid`, `picture`) " +
        "VALUES (NULL, '" + data.id + "', '" + data.images + "');"
, function (err, rows, fields) {
        if (!err) {
            res.end("success");
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
});

router.post('/pictureload', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT `o_a_pictureid` as 'pictureid', `picture` as 'activityPics' FROM `o_a_pictures` WHERE `activityid` = '"+data.id+"';"
, function (err, rows, fields) {
        if (!err) {
            var data = rows;
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
});

router.post('/activityupdate', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.o_activity SET o_activity.title='"+data.title+"' , o_activity.posttime='"+data.time+"' ,o_activity.picture='"+data.images+"',o_activity.video='"+data.video+"',o_activity.article='"+data.content+"' WHERE o_activity.activityid = '"+data.id+"';"
, function (err, rows, fields) {
        if (!err) {
            res.json([{ "a_id": data.id }]);
            res.end();
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
    
});

router.post('/activityupdatePublish', function (req, res) {
    var db = req.db;
    var data = req.body;
    if (data.activityPublish == 'false') { data.activityPublish = '0'; }
    else { data.activityPublish = '1'; }
    console.log(data);
    db.query("UPDATE yenbao.o_activity SET  o_activity.posttime = '" + data.activityDate + "' ,o_activity.statue = '" + data.activityPublish + "' WHERE o_activity.activityid = '" + data.activityID + "';"
, function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');console.log(err); res.end();
        }
    });
});

router.post('/activitydelete', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `o_activity` WHERE `activityid` = '" + id + "';", function (err, rows, fields) {
        if (!err) {
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/activitydeletepic', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    console.log(id);
    db.query("DELETE FROM `o_a_pictures` WHERE `o_a_pictureid` = '" + id + "';", function (err, rows, fields) {
        if (!err) {
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
module.exports = router;

﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var multer = require('multer');
var uploading = multer({ dest: './public/images/uploads/' });
var path = require('path');
var fs = require('fs');
// function to encode file data to base64 encoded string
function base64_encode(file) {
    // read binary data
    var bitmap = fs.readFileSync(file);
    // convert binary data to base64 encoded string
    return new Buffer(bitmap, 'binary').toString('base64');
}

router.post('/member', function (req, res) {
    var db = req.db;
    var data = req.body;
    var MW = '';
    var CW = '';
    if (data.c_type == 1) { MW = 'FROM memberlist;' }
    else if (data.c_type == 2) { MW = 'FROM memberlist,companylist WHERE memberlist.companyid = companylist.companyid AND companylist.type = 3 OR memberlist.companyid = companylist.companyid AND companylist.companyid =' + data.c_id + ';' }
    else if (data.c_type == 3) { MW = 'FROM memberlist,companylist WHERE memberlist.companyid = companylist.companyid AND companylist.companyid =' + data.c_id + ';' }
    if (data.c_type == 1) { CW = 'FROM companylist;' }
    else if (data.c_type == 2) { CW = 'FROM companylist WHERE companylist.type = 3 OR companylist.companyid =' + data.c_id + ';' }
    else if (data.c_type == 3) { CW = 'FROM companylist WHERE  companylist.companyid =' + data.c_id + ';' }
    var sqls = {
        levels: "SELECT m_level.m_levelid as 'LevelId' , m_level.level as 'Level' FROM m_level;",
        members: "SELECT memberlist.memberid as 'Id',memberlist.m_levelid as 'Level', memberlist.name as 'Name',memberlist.tel as 'Phone', memberlist.email as 'Email', memberlist.account as 'Account', memberlist.password as 'Password', memberlist.dividend as 'Dividend', memberlist.companyid as 'Store', memberlist.statue as 'Statue' " + MW ,
        company:"SELECT companylist.companyid as 'StoreId' , companylist.name as 'Store' "+ CW
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.end(err);
        } else {
            var tdata = results;
            var j = 1;
            var levels = [{ 'Level': '所有等級', 'LevelId': 0 }];
            for (var i in tdata.levels) {
                levels[j] = tdata.levels[i];
                j++;
            }
            tdata.levels = levels;
            if (data.c_type == 1) {
                var c = 2;
                var company = [{ 'Store': '所有店家', 'StoreId': 0 }, { 'Store': '網路線上註冊', 'StoreId': -1 }];
                for (var i in tdata.company) {
                    company[c] = tdata.company[i];
                    c++;
                }
                tdata.company = company;
            }
            else {
                var c = 1;
                var company = [{ 'Store': '所有店家', 'StoreId': 0 }];
                for (var i in tdata.company) {
                    company[c] = tdata.company[i];
                    c++;
                }
                tdata.company = company;
            }
            for (var i in tdata.members) {
                if (tdata.members[i].Store == null) { tdata.members[i].Store = -1 }
            }
            res.json(tdata);
            res.end();
        }
    });
});

router.post('/memberdata', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT memberlist.companyid as 'cid' FROM memberlist WHERE memberlist.memberid = '"+data.id+"';"
        , function (err, rows, fields) {
        if (!err) {
            var d = rows;
            if (d[0].cid == null) {
                db.query("SELECT memberlist.memberid as 'm_id', memberlist.companyid as 'm_company', memberlist.signupdate as 'date', memberlist.name as 'm_name', TO_BASE64(memberlist.picture) as 'm_picture', memberlist.m_levelid as 'm_level', memberlist.tel as 'm_phone', memberlist.gender as 'm_gender', memberlist.birthday as 'm_birthday',memberlist.email as 'm_email',memberlist.postalcode as 'm_zipcode',memberlist.city as 'm_city',memberlist.township as 'm_township', memberlist.address as 'm_address', memberlist.account as 'm_account', memberlist.password as 'm_password' , memberlist.dividend as 'm_dividend', memberlist.statue as 'm_statue' FROM memberlist WHERE memberlist.memberid = '" + data.id + "';"
                , function (err, rows, fields) {
                    var tdata = rows[0];
                    tdata.m_date = new Date(tdata.date).toLocaleTimeString();
                    tdata.m_company = '網路線上註冊';
                    res.json(tdata);
                    res.end();
                });
            }
            else {
                db.query("SELECT memberlist.memberid as 'm_id', companylist.name as 'm_company', memberlist.signupdate as 'date', memberlist.name as 'm_name', TO_BASE64(memberlist.picture) as 'm_picture', memberlist.m_levelid as 'm_level', memberlist.tel as 'm_phone', memberlist.gender as 'm_gender', memberlist.birthday as 'm_birthday',memberlist.email as 'm_email',memberlist.postalcode as 'm_zipcode',memberlist.city as 'm_city',memberlist.township as 'm_township', memberlist.address as 'm_address', memberlist.account as 'm_account', memberlist.password as 'm_password',memberlist.dividend as 'm_dividend', memberlist.statue as 'm_statue' FROM memberlist, companylist WHERE memberlist.companyid = companylist.companyid AND memberlist.memberid = '"+data.id+"';"
                , function (err, rows, fields) {
                    var tdata = rows[0];
                    tdata.m_date = new Date(tdata.date).toLocaleTimeString();
                    res.json(tdata);
                    res.end();
                });
            }
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});
//上傳照片
router.post('/insertpic', uploading.single('uploadImg'), function (req, res) {
    var db = req.db;
    var data = req.file;
    console.log(data);
    var img = req.file.originalname.substr(-4);
    res.json({ 'name': req.file.filename + img });
    fs.rename('./public/images/uploads/' + req.file.filename, './public/images/uploads/' + req.file.filename + img, function (err) {
        if (err) throw err
        console.log('File saved.')
    })
    res.end();
});
router.post('/memberinsert', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("select memberlist.account as 'ngid' from memberlist where memberlist.account = '" + data.m_account + "';" , 
                function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                res.send('IDrepeat');
                res.end();
            }
            else {
                var PIC = "";
                if (data.m_picture == null || data.m_picture == '') { PIC = null; }
                else { PIC = "FROM_BASE64('" + base64_encode(data.m_picture) + "')"; }
                db.query("INSERT INTO `memberlist` (`memberid`, `name`, `tel`, `companyid`, `gender`, `birthday`, `email`, `postalcode`, `city`, `township`, `address`, `picture`, `account`, `password`, `dividend`, `m_levelid`, `signupdate`, `statue`) " +
                    " VALUES (NULL, '"+data.m_name+"', '"+data.m_phone+"', '"+data.m_company+"', '"+data.m_gender+"', '"+data.m_birthday+"', '"+data.m_email+"', '" + data.m_postalcode + "', '" + data.m_city + "', '" + data.m_township + "', '"+data.m_address+"', "+PIC+", '"+data.m_account+"', '"+data.m_password+"', '"+data.m_dividend+"', '"+ data.m_level+"', CURRENT_TIMESTAMP, '"+data.m_statue+"')"
                    , function (err, rows, fields) {
                    if (!err) {
                        console.log('success insert');
                        res.end('success');
                    }
                    else {
                        console.log('Error while performing Query.');
                        res.end();
                    }
                });
            }
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});

router.post('/memberupdate', function (req, res) {
    var db = req.db;
    var data = req.body;
    console.log(data);
    db.query("select memberlist.account as 'ngid' from memberlist where memberlist.account = '" + data.m_account + "';" , 
                function (err, rows, fields) {
        if (!err) {
            if (rows[0] && rows[0].ngid != data.m_account) {
                res.send('IDrepeat');
                res.end();
            }
            else {
                var PIC = "";
                if(data.m_picture == null || data.m_picture == '') { PIC = ""; }
                else { PIC = ", memberlist.picture = FROM_BASE64('" + base64_encode(data.m_picture) + "')"; }
                db.query("UPDATE yenbao.memberlist SET memberlist.name ='"+data.m_name+"', memberlist.tel ='"+ data.m_phone+"', memberlist.gender ='"+data.m_gender+"', memberlist.birthday ='"+data.m_birthday+"', memberlist.postalcode ='"+data.m_postalcode+"', memberlist.city ='"+ data.m_city+"', memberlist.township ='" + data.m_township + "', memberlist.address ='" + data.m_address + "' "+PIC+", memberlist.account ='"+data.m_account+"', memberlist.password ='"+data.m_password+"', memberlist.dividend ='"+data.m_dividend+"', memberlist.m_levelid ='"+data.m_level+"', memberlist.signupdate = '"+data.m_date+"', memberlist.statue ='"+data.m_statue+"' WHERE memberlist.memberid = '"+data.m_id+"';"
                    , function (err, rows, fields) {
                    if (!err) {
                        console.log('success insert');
                        res.end('success');
                    }
                    else {
                        console.log('Error while performing Query.');
                        res.end();
                    }
                });
            }
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});

//loadselecttype
router.get('/loadselectlevel', function (req, res) {
    var db = req.db;
    db.query("SELECT m_level.m_levelid as 'LevelId' , m_level.level as 'Level' FROM m_level;"
        , function (err, rows, fields) {
        if (!err) {
            var tdata = rows;
            res.json(tdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});
router.get('/loadlevel', function (req, res) {
    var db = req.db;
    db.query("SELECT m_level.m_levelid as 'LevelId' , m_level.level as 'Level' , m_level.competence as 'LevelCompetence' FROM m_level;"
        , function (err, rows, fields) {
        if (!err) {
            var tdata = rows;
            res.json(tdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});

router.post('/insertlevel', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("INSERT INTO `m_level` (`m_levelid`, `level`, `competence`) VALUES (NULL, '"+data.level+"', '');"
        , function (err, rows, fields) {
        if (!err) {
            res.end('success');
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});

router.post('/updatelevel', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.m_level SET m_level.level ='" + data.Level + "' , m_level.competence ='" + data.LevelCompetence + "' WHERE m_level.m_levelid = '" + data.LevelId + "';"
, function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/purchase', function (req, res) {
    var db = req.db;
    var data = req.body;
    var MW = '';
    if (data.c_type == 1) { MW = 'FROM m_deal,memberlist,p_data,productlist WHERE m_deal.memberid = memberlist.memberid AND m_deal.productid = productlist.productid AND productlist.p_dataid = p_data.p_dataid ;' }
    else if (data.c_type == 2) { MW = 'FROM m_deal,memberlist,p_data,productlist,companylist WHERE m_deal.memberid = memberlist.memberid AND m_deal.productid = productlist.productid AND productlist.p_dataid = p_data.p_dataid AND m_deal.companyid = companylist.companyid AND companylist.type = 3 OR m_deal.memberid = memberlist.memberid AND m_deal.productid = productlist.productid AND productlist.p_dataid = p_data.p_dataid AND m_deal.companyid = companylist.companyid AND m_deal.companyid =' + data.c_id + ';' }
    else if (data.c_type == 3) { MW = 'FROM m_deal,memberlist,p_data,productlist,companylist WHERE m_deal.memberid = memberlist.memberid AND m_deal.productid = productlist.productid AND productlist.p_dataid = p_data.p_dataid AND m_deal.companyid = companylist.companyid AND m_deal.companyid =' + data.c_id + ';' }
    var sqls = {
        levels: "SELECT m_level.m_levelid as 'MemberLevelId' , m_level.level as 'MemberLevel' FROM m_level;",
        purchase: "SELECT m_deal.dealid as 'PurchaseId',m_deal.companyid as 'c_id', m_deal.memberid as 'MemberId', memberlist.name as 'MemberName', memberlist.m_levelid as 'MemberLevel', m_deal.date as 'Date', m_deal.productid as 'ProductId', p_data.name as 'ProductName', productlist.p_typeid as 'ProductType', m_deal.amount as 'ProductNum', m_deal.totalprice as 'ProductCost', m_deal.dividend as 'Dividend', m_deal.returnornot as 'Return',m_deal.hypermyopia as 'hypermyopia' " + MW ,
        product: "select p_type.type as 'ProductType', p_type.p_typeid as 'ProductTypeId' from p_type;",
        company :"SELECT companylist.companyid as 'StoreId' , companylist.name as 'Store' FROM companylist;"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.end(err);
        } else {
            var tdata = results;
            var j = 1;
            var levels = [{ 'MemberLevel': '所有等級', 'MemberLevelId': 0 }];
            for (var i in tdata.levels) {
                levels[j] = tdata.levels[i];
                j++;
            }
            tdata.levels = levels;
            var c = 1;
            var product = [{ 'ProductType': '所有類別', 'ProductTypeId': 0 }];
            for (var i in tdata.product) {
                product[c] = tdata.product[i];
                c++;
            }
            tdata.product = product;
            for (var i in tdata.purchase) {
                tdata.purchase[i].PurchaseDate = new Date(tdata.purchase[i].Date).toLocaleTimeString();
            }
            var c = 2;
            var company = [{ 'Store': '所有店家', 'StoreId': 0 }, { 'Store': '網路購物', 'StoreId': -1 }];
            for (var i in tdata.company) {
                company[c] = tdata.company[i];
                c++;
            }
            tdata.company = company;
            res.json(tdata);
            res.end();
        }
    });
});
router.post('/purchasehypermyopia', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT m_deal.hypermyopia as 'FarNear', m_deal.rightOD as 'Rod', m_deal.leftOD as 'Lod', m_deal.PD as 'Pd', m_deal.specialprocess as 'SpecProcess', m_deal.remark as 'Remark' FROM m_deal WHERE m_deal.dealid = '"+data.id+"';"
        , function (err, rows, fields) {
        if (!err) {
            var tdata = rows[0];
            res.json(tdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});

router.post('/memberpurchase', function (req, res) {
    var db = req.db;
    var data = req.body;
    var sqls = {
        levels: "SELECT m_level.m_levelid as 'MemberLevelId' , m_level.level as 'MemberLevel' FROM m_level;",
        purchase: "SELECT m_deal.dealid as 'PurchaseId',m_deal.companyid as 'c_id', m_deal.memberid as 'MemberId', memberlist.name as 'MemberName', memberlist.m_levelid as 'MemberLevel', m_deal.date as 'Date', m_deal.productid as 'ProductId', p_data.name as 'ProductName', productlist.p_typeid as 'ProductType', m_deal.amount as 'ProductNum', m_deal.totalprice as 'ProductCost', m_deal.dividend as 'Dividend', m_deal.returnornot as 'Return',m_deal.hypermyopia as 'hypermyopia' FROM m_deal,memberlist,p_data,productlist WHERE m_deal.memberid = memberlist.memberid AND m_deal.productid = productlist.productid AND productlist.p_dataid = p_data.p_dataid AND m_deal.memberid = '"+data.m_id+"';" ,
        product: "select p_type.type as 'ProductType', p_type.p_typeid as 'ProductTypeId' from p_type;",
        member : "SELECT memberlist.postalcode as 'zipcode' , memberlist.city as 'city' , memberlist.township as 'township' , memberlist.address as 'address' , memberlist.dividend as 'dividend' FROM memberlist WHERE memberlist.memberid = '" + data.m_id + "';",
        price : "SELECT SUM(m_deal.totalprice) as 'price' FROM m_deal WHERE m_deal.returnornot = '0' AND m_deal.memberid ='" + data.m_id + "';",
        company : "SELECT companylist.companyid as 'StoreId' , companylist.name as 'Store' FROM companylist;"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.end(err);
        } else {
            var tdata = results;
            var j = 1;
            var levels = [{ 'MemberLevel': '所有等級', 'MemberLevelId': 0 }];
            for (var i in tdata.levels) {
                levels[j] = tdata.levels[i];
                j++;
            }
            tdata.levels = levels;
            var c = 1;
            var product = [{ 'ProductType': '所有類別', 'ProductTypeId': 0 }];
            for (var i in tdata.product) {
                product[c] = tdata.product[i];
                c++;
            }
            tdata.product = product;
            var c = 2;
            var company = [{ 'Store': '所有店家', 'StoreId': 0 }, { 'Store': '網路購物', 'StoreId': -1 }];
            for (var i in tdata.company) {
                company[c] = tdata.company[i];
                c++;
            }
            tdata.company = company;
            for (var i in tdata.purchase) {
                tdata.purchase[i].PurchaseDate = new Date(tdata.purchase[i].Date).toLocaleTimeString();
            }
            if (tdata.price[0].price == null) { tdata.price[0].price = 0; }
            res.json(tdata);
            res.end();
        }
    });
});

router.post('/loadhealth', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    var sqls = {
        health: "SELECT m_health.healthid as 'healthid' , m_health.memberid as 'memberid' , companylist.name as 'branch' , m_health.starttime as 'Stime' , m_health.endtime as 'endtime' , m_health.charge as 'charge' FROM m_health , companylist WHERE m_health.companyid = companylist.companyid AND `memberid` = '"+id+"';" ,
        member : "SELECT memberlist.postalcode as 'zipcode' , memberlist.city as 'city' , memberlist.township as 'township' , memberlist.address as 'address' , memberlist.dividend as 'dividend' FROM memberlist WHERE memberlist.memberid = '" + id + "';",
        time : "SELECT COUNT(m_health.healthid) AS 'time' FROM m_health WHERE m_health.memberid = '"+id+"';"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.end(err);
        } else {
            var tdata = results;
            for (var i in tdata.health) {
                var dt1 = new Date(tdata.health[i].starttime);
                var dt2 = new Date(tdata.health[i].endtime);
                var time = (dt2 - dt1) / (1000);
                var timeS = time % 60;
                var timeM = parseInt(time / 60);
                tdata.health[i].time = timeM + "分鐘" + timeS + "秒";
                tdata.health[i].timeSS = time;
                tdata.health[i].starttime = new Date(tdata.health[i].Stime).toLocaleTimeString();
                tdata.health[i].endtime = new Date(tdata.health[i].endtime).toLocaleTimeString();
            }
            res.json(tdata);
            res.end();
        }
    });
});
router.post('/inserthealth', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("INSERT INTO `m_health` (`healthid`, `memberid`, `companyid`, `starttime`, `endtime`, `charge`) VALUES (NULL, '" + data.m_id + "', '" + data.c_id + "', '" + data.h_date + " " + data.h_stime + "', '" + data.h_date + " " + data.h_etime + "', '" + data.h_people + "')"
        , function (err, rows, fields) {
        if (!err) {
            res.end('success');
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});
router.post('/updatehealth', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.m_health SET m_health.companyid ='" + data.c_id + "', m_health.starttime ='" + data.date + " " + data.starttime + "', m_health.endtime ='" + data.date + " " + data.endtime + "', m_health.charge ='" + data.charge + "' WHERE m_health.healthid = '" + data.healthid + "';"
        , function (err, rows, fields) {
        if (!err) {
            res.json(data);
            res.end('success');
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});

router.post('/deletehealth', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `m_health` WHERE `healthid` ='" + id + "'", function (err, rows, fields) {
        if (!err) {
            console.log('success delete ' + id);
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
router.post('/loadscreen', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    var sqls = {
        screen: "SELECT m_screen.m_screenid as 'm_screenid',m_screen.memberid as 'memberid',companylist.companyid as 'c_id',companylist.name as 'branch',m_screen.date as 'date' , m_screen.rightvision as 'Reye' ,m_screen.leftvision as 'Leye' , m_screen.pd as 'pd' FROM m_screen , companylist WHERE m_screen.companyid = companylist.companyid AND m_screen.memberid = '" + id + "';" ,
        member : "SELECT memberlist.postalcode as 'zipcode' , memberlist.city as 'city' , memberlist.township as 'township' , memberlist.address as 'address' , memberlist.dividend as 'dividend' FROM memberlist WHERE memberlist.memberid = '" + id + "';",
        time : "SELECT COUNT(m_screen.m_screenid) AS 'time' FROM m_screen WHERE m_screen.memberid = '" + id + "';"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.end(err);
        } else {
            var tdata = results;
            for (var i in tdata.screen) {
                tdata.screen[i].mydate = new Date(tdata.screen[i].date).toLocaleTimeString();
            }
            res.json(tdata);
            res.end();
        }
    });
});
router.post('/insertscreen', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("INSERT INTO `m_screen` (`m_screenid`, `memberid`, `companyid`, `date`, `rightvision`, `leftvision`, `pd`) VALUES (NULL, '"+data.m_id+"','"+data.c_id+"', CURRENT_TIMESTAMP, '"+data.s_RSPH+" "+data.s_RCYL+" "+data.s_RAX+"', '" + data.s_LSPH + " " + data.s_LCYL + " " + data.s_LAX + "', '"+data.s_PD+"')"
        , function (err, rows, fields) {
        if (!err) {
            res.end('success');
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});
router.post('/updatescreen', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE yenbao.m_screen SET m_screen.date ='"+data.date+"', m_screen.rightvision ='" + data.s_RSPH + " " + data.s_RCYL + " " + data.s_RAX + "', m_screen.leftvision ='" + data.s_LSPH + " " + data.s_LCYL + " " + data.s_LAX + "',`pd`='" + data.s_PD + "' WHERE m_screen.m_screenid = '"+data.s_id+"';"
        , function (err, rows, fields) {
        if (!err) {
            res.end('success');
        }
        else {
            console.log('Error while performing Query.');
            res.end();
        }
    });
});

router.post('/deletescreen', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `m_screen` WHERE `m_screenid` ='" + id + "'", function (err, rows, fields) {
        if (!err) {
            console.log('success delete ' + id);
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

module.exports = router;
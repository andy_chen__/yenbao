﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');

router.get('/headquaterstock', function (req, res) {
    var db = req.db;
    db.query("select p_type.type as `type`,p_data.name as `name`,p_data.price as `price`,productlist.stock as `amount`,p_data.picture as `picture` from p_type, p_data, productlist where productlist.p_dataid = p_data.p_dataid AND productlist.p_typeid = p_type.p_typeid;"
        , function (err, rows, fields) {
        if (!err) {
            var data = rows;
            for (var i in data) {
                data[i].picture = "./images/product/" + data[i].picture ;
            }
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/headquatership', function (req, res) {
    var db = req.db;
    var data = req.body;
    var shipsqls = {
        h1 :"INSERT INTO `yenbao`.`headquater_purchase` (`purchaseid`, `companyid`, `date`, `productid`, `amount`, `totalprice`, `returned`) VALUES(NULL, '" + data.c_no + "', '" + data.date + "' , '" + data.p_no + "', '" + data.amount + "', '" + data.total + "', '" + data.return + "');",
        h2 :"UPDATE yenbao.branches_productlist SET branches_productlist.stock = branches_productlist.stock + '"+data.amount+"' WHERE branches_productlist.companyid = '" + data.c_no + "' AND branches_productlist.productid = '" + data.p_no + "';"
    };
    async.map(shipsqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
            res.end();
        } else {
            res.send("headquatership success");
            res.end();
        }
    });
});


router.post('/headgetreturn', function (req, res) {
    var db = req.db;
    var data = req.body;
    var returnsqls = {
        h1 : "UPDATE yenbao.headquater_purchase SET headquater_purchase.returned= '1' WHERE headquater_purchase.companyid = '" + data.c_no + "' AND headquater_purchase.purchaseid = '" + data.p_no + "';",
        h2 : "INSERT INTO `yenbao`.`h_return` (`h_returnid`, `purchaseid`, `date`, `amount`, `totalprice`) VALUES(NULL, '" + data.p_no + "', '" + data.date + "', '" + data.amount + "', '" + data.total + "');"
    };
    async.map(returnsqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
            res.end();
        } else {
            res.send("headgetreturn success");
            res.end();
        }
    });
});

router.post('/companystockcheck', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("select p_type.type as `type`, p_data.name as `name`, branches_productlist.stock as `amount`, p_data.price as `price`, p_data.picture as `picture` from p_type, branches_productlist, productlist, p_data where branches_productlist.productid = productlist.productid AND productlist.p_typeid = p_type.p_typeid AND productlist.p_dataid = p_data.p_dataid AND branches_productlist.productid = '"+id+"';"
        , function (err, rows, fields) {
        if (!err) {
            var data = rows;
            for (var i in data) {
                data[i].picture = "./images/product/" + data[i].picture;
                data[i].totalprice = (parseFloat(data[i].price) * parseFloat(data[i].amount));
            }
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/headquateradd', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("select headquater_purchase.date as `p_date`, p_type.type as `type`, p_data.name as `name`, headquater_purchase.amount as `amount`, p_data.price as `price`, headquater_purchase.totalprice as `totalprice`, headquater_purchase.returned as `return` from headquater_purchase, p_type, productlist, p_data, companylist where headquater_purchase.companyid = '" + data.id + "' AND headquater_purchase.date BETWEEN '"+data.firstdate+"' AND '" + data.lastdate + "' AND headquater_purchase.productid = productlist.productid AND productlist.p_typeid = p_type.p_typeid AND productlist.p_dataid = p_data.p_dataid AND headquater_purchase.companyid = companylist.companyid"
        , function (err, rows, fields) {
        if (!err) {
            var rdata = rows;
            res.json(rdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.get('/headquaterreturn', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("select h_return.date as `r_date`, p_type.type as `type`, p_data.name as `name`, h_return.amount as `amount`, p_data.price as `price`, h_return.totalprice as `totalprice` from headquater_purchase, p_data, p_type, h_return, productlist where h_return.date BETWEEN '" + data.firstdate + "' AND '"+data.lastdate+"' AND h_return.purchaseid = headquater_purchase.purchaseid AND headquater_purchase.productid = productlist.productid AND productlist.p_dataid = p_data.p_dataid AND productlist.p_typeid = p_type.p_typeid"
        , function (err, rows, fields) {
        if (!err) {
            var rdata = rows;
            res.json(rdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

//router.post('/producttypesearch', function (req, res) {
//    var db = req.db;
//    var data = req.body;
//    db.query("SELECT `p_typeid` as 'ptypeID', `mtypeid` as 'ptypeMID', `type` as 'ptypeName', `level` as 'ptypeLevel', `picture` as 'ptypePicture' FROM `p_type` WHERE `level` = '"+data.level+"' or `mtypeid` = '"+data.typeid+"' or `type` = '%"+data.type+"%'"
//        , function (err, rows, fields) {
//        if (!err) {
//            var rdata = rows;
//            for (var i in rdata) {
//                rdata[i].picture = "./images/product/" + rdata[i].picture;
//            }
//            res.json(rdata);
//            res.end();
//        }
//        else {
//            console.log('Error while performing Query.');
//        }
//    });
//});


router.post('/productbranchsearch', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT `companyid` as 'companyID', `type` as 'companyType', `name` as 'companyName', `c_dataid` as 'companyDataid', `c_account` as 'companyAccount' FROM `companylist` WHERE `name` = '%"+data.name+"%' or `c_account` = '%"+data.ID+"%'"
        , function (err, rows, fields) {
        if (!err) {
            var rdata = rows;
            res.json(rdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
module.exports = router;
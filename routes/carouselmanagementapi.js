﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var multer = require('multer');
var uploading = multer({ dest: './public/images/uploads/' });
var path = require('path');
var fs = require('fs');
router.get('/carousel', function (req, res) {
    var db = req.db;   
    db.query("select o_carousel.carouselid as 'id', o_carousel.uploaddate as 'time', o_carousel.uploaddate as 'ImgDate', o_carousel.uploaddate as 'ImgTime', o_carousel.picture as 'ImgSrc', o_carousel.Url as 'ImgUrl', o_carousel.priority as 'ImgPriority' , o_carousel.statue as 'ImgCarousel' from o_carousel order by o_carousel.priority ASC;"
        , function (err, rows, fields) {
        if (!err) {
            var data = rows;
            for (var i in data) {
                data[i].ImgTime = new Date(data[i].ImgTime).toLocaleTimeString();
            }
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
        
    });
});

router.post('/insertpic', uploading.single('C_uploadCoverImg'), function (req, res) {
    var db = req.db;
    var img = req.file.originalname.substr(-4);
    res.json({ 'name': req.file.filename + img });
    fs.rename('./public/images/uploads/' + req.file.filename, './public/images/carousel/' + req.file.filename + img, function (err) {
        if (err) throw err
        console.log('File saved.')
    })
    res.end();
});

router.post('/insertItem', function (req, res){
    var db = req.db;
    var data = req.body;
    if (data.ImgPriority == '' || data.images == '') {
        res.send('資料不完整'); res.end();
    }
    else {
        db.query("INSERT INTO `yenbao`.`o_carousel` (`carouselid`, `picture`, `Url`, `uploaddate`, `statue`, `priority`) VALUES(NULL, '"+ data.images+"', '" + data.ImgUrl + "', CURRENT_TIMESTAMP, '1', '" + data.ImgPriority + "');"
            , function (err, rows, fields) {
            if (!err) {
                console.log("success insert");
                res.send("success insert");
                res.end();
            }
            else {
                console.log('Error while performing Query.'); res.end();
            }
        });
    }
});

router.post('/updateItem', function (req, res) {
    var db = req.db;
    var data = req.body;
    if (data.ImgCarousel == 'false') { data.ImgCarousel = 0; }
    else { data.ImgCarousel= 1; }
    data.time = data.ImgDate + " " + data.ImgTime;
    db.query("UPDATE yenbao.o_carousel SET o_carousel.uploaddate = '" + data.time + "', o_carousel.Url = '" + data.ImgUrl + "' , o_carousel.priority ='" + data.ImgPriority + "', o_carousel.statue ='" + data.ImgCarousel + "' WHERE o_carousel.carouselid = '" + data.id + "';"
   , function (err, rows, fields) {
        if (!err) {
            console.log("success update");
            data.ImgTime = new Date(data.time).toLocaleTimeString();
            res.json(data);
            res.end();
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
    
});

router.post('/deleteItem', function (req, res) {
    var db = req.db;
    var id = req.body.id;
    db.query("DELETE FROM `o_carousel` WHERE `carouselid` = '" + id + "';", function (err, rows, fields) {
        if (!err) {
            res.end('success delete');
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

module.exports = router;
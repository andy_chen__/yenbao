﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');

router.post('/companyrecords', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT * FROM `m_deal` WHERE m_deal.companyid = '"+data.c_id+"' ORDER BY m_deal.date DESC;"
        , function (err, rows, fields) {
        if (!err) {
            var rdata = rows;
            for (var i in rdata) {
                rdata[i].date = new Date(rdata[i].date).toLocaleString();
            }
            res.json(rdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
});

router.post('/companysales', function (req, res) {
    var db = req.db;
    var data = req.body;
    console.log(data);
    var hypermyopia, rightOD, leftOD, PD, specialprocess, remark;
    if (data.hypermyopia == 'null') { hypermyopia = null; } else { hypermyopia = "'"+hypermyopia+"'"; }
    if (data.rightOD == 'null') { rightOD = null; } else { rightOD = "'"+rightOD+ "'"; }
    if (data.leftOD == 'null') { leftOD = null; } else { leftOD = "'"+leftOD+"'"; }
    if (data.PD == 'null') { PD = null; } else {PD =  "'"+PD+ "'"; }
    if (data.specialprocess == 'null') { specialprocess = null; } else { specialprocess = "'"+specialprocess+ "'"; }
    if (data.remark == 'null') { remark = null; } else { remark = "'"+remark+"'"; }
    db.query("INSERT INTO `m_deal` (`dealid`, `memberid`, `companyid`, `productid`, `date`, `amount`, `returnornot`, `hypermyopia`, `rightOD`, `leftOD`, `PD`, `specialprocess`, `remark`, `totalprice`, `dividend`) "+
    "VALUES(NULL, '"+data.memberid+"', '" + data.companyid + "', '" + data.productid + "', '"+data.date+"', '"+data.amount+"', '0', "+hypermyopia+", "+rightOD+", "+leftOD+", "+PD+", "+specialprocess+", "+remark+", '"+data.totalprice+"', '"+data.dividend+"'); "
        , function (err, rows, fields) {
        if (!err) {
            if (data.memberid != '') {
                db.query("UPDATE memberlist SET memberlist.dividend = memberlist.dividend + '" + data.dividend + "' WHERE memberlist.memberid = '" + data.memberid + "';"
              , function (err, rows, fields) {
                    if (!err) {
                        res.send("companysales success");
                        res.end();
                    }
                    else {
                        console.log('Error while performing Query.'); res.end();
                    }
                });
            }
            else {
                res.send("companysales success");
                res.end();
            }
            
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
    
});

router.post('/companyreturnback', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("UPDATE `m_deal` SET `returnornot`='1' WHERE `dealid` = '"+data.s_id+"'"
        , function (err, rows, fields) {
        if (!err) {
            if (data.memberid != '') {
                db.query("UPDATE memberlist SET memberlist.dividend = memberlist.dividend - '" + data.dividend + "' WHERE memberlist.memberid = '" + data.memberid + "';"
              , function (err, rows, fields) {
                    if (!err) {
                        res.send("companyreturnback success");
                        res.end();
                    }
                    else {
                        console.log('Error while performing Query.'); res.end();
                    }
                });
            }
            else {
                res.send("companyreturnback success");
                res.end();
            }
            
        }
        else {
            console.log('Error while performing Query.'); res.end();
        }
    });
    
});
router.post('/memberdata', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT * FROM `memberlist` WHERE memberlist.tel = '" + data.tel + "';"
        , function (err, rows, fields) {
        if (!err) {
            var rdata = rows;
            for (var i in rdata) {
                rdata[i].signupdate = new Date(rdata[i].signupdate).toLocaleString();
                rdata[i].birthday = new Date(rdata[i].birthday).toLocaleDateString();
            }
            res.json(rdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
//用id查會員
router.post('/memberdatasearch', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT * FROM `memberlist` WHERE memberlist.memberid = '"+data.m_id+"';"
        , function (err, rows, fields) {
        if (!err) {
            var rdata = rows;
            for (var i in rdata) {
                rdata[i].signupdate = new Date(rdata[i].signupdate).toLocaleString();
                rdata[i].birthday = new Date(rdata[i].birthday).toLocaleDateString();
            }
            res.json(rdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/memberdataadd', function (req, res) {
    var db = req.db;
    var data = req.body;
    console.log(data);
    var sqls = {
        account: "SELECT memberlist.memberid FROM memberlist WHERE memberlist.account = '" + data.account + "';"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
            console.log('Error while performing Query.');
            res.end();
        } else {
            
            if (results.account[0]) {
                res.end("accountrepeat");
            }
            else {
                var pic = data.picture;
                if (pic == 'null') { pic = null; }
                else { pic = "'"+pic+"'"; }
                db.query("INSERT INTO `memberlist` (`memberid`, `name`, `tel`, `companyid`, `gender`, `birthday`, `email`, `postalcode`, `city`, `township`, `address`, `picture`, `account`, `password`, `dividend` , `m_levelid`, `signupdate`, `statue`)" +
                           " VALUES (NULL, '" + data.name + "','" + data.tel + "','" + data.companyid + "','" + data.gender + "','" + data.birthday + "','" + data.email + "','" + data.postalcode + "','" + data.city + "','" + data.township + "','" + data.address + "',"+pic+",'" + data.account + "','" + data.password + "','0','1','" + data.signupdate + "','0');"
                 , function (err, rows, fields) {
                    if (!err) {
                        res.send("memberdataadd success");
                        res.end();
                    }
                    else {
                        res.send(err);
                        console.log('Error while performing Query.');
                        res.end();
                    }
                });
            }
        }
    });

    
});

router.post('/memberscreen', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT * FROM `m_screen` WHERE `memberid`= '"+data.m_id+"';"
        , function (err, rows, fields) {
        if (!err) {
            var rdata = rows;
            for (var i in rdata) {
                rdata[i].date = new Date(rdata[i].date).toLocaleDateString();
            }
            res.json(rdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/memberscreenadd', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query(" INSERT INTO `m_screen` (`m_screenid`, `memberid`, `companyid`, `date`, `rightvision`, `leftvision`, `pd`)" +
        " VALUES (NULL, '"+data.memberid+"','"+data.companyid+"','"+data.date+"','"+data.rightvision+"','"+data.leftvision+"','"+data.pd+"');"
        , function (err, rows, fields) {
        if (!err) {
            res.send("memberscreenadd success");
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/memberhealth', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT * FROM `m_health` WHERE `memberid` = '" + data.m_id + "';"
        , function (err, rows, fields) {
        if (!err) {
            var rdata = rows;
            for (var i in rdata) {
                rdata[i].starttime = new Date(rdata[i].starttime).toLocaleString();
                rdata[i].endtime = new Date(rdata[i].endtime).toLocaleString();
            }
            res.json(rdata);
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/memberhealthadd', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("INSERT INTO `m_health` (`healthid`, `memberid`, `companyid`, `starttime`, `endtime`, `charge`)" +
        " VALUES (NULL, '"+data.memberid+"','"+data.companyid+"','"+data.starttime+"','"+data.endtime+"','"+data.charge+"');"
        , function (err, rows, fields) {
        if (!err) {
            res.send("memberhealthadd success");
            res.end();
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
module.exports = router;
﻿var express = require('express');
var router = express.Router();
var mysql = require('mysql');
var async = require('async');
var multer = require('multer');
var uploading = multer({ dest: './public/images/uploads/' });
var path = require('path');
var fs = require('fs');
var googleMapsClient = require('@google/maps').createClient({
    key: 'AIzaSyBcM7UtSWnCCg06uGk6jklRf99EFndQJjw'//google key
});
router.get('/membercount', function (req, res) {
    var db = req.db;
    db.query('SELECT COUNT(memberid) as "count" FROM memberlist'
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                console.log(rows[0].count);
                res.json(rows[0]);
                res.end();
            }
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.get('/salecount', function (req, res) {
    var db = req.db;
    db.query('SELECT COUNT(dealid) as "count" FROM m_deal'
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                console.log(rows[0].count);
                res.json(rows[0]);
                res.end();
            }
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.get('/saletotal', function (req, res) {
    var db = req.db;
    db.query("SELECT SUM(totalprice) as 'total' FROM m_deal WHERE m_deal.date"
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                console.log(rows[0].total);
                res.json(rows[0]);
                res.end();
            }
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.get('/branchcount', function (req, res) {
    var db = req.db;
    db.query('SELECT COUNT(companylist.companyid) as "count" FROM companylist'
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                console.log(rows[0].count);
                res.json(rows[0]);
                res.end();
            }
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.get('/salePftype', function (req, res) {
    var db = req.db;
    var sqls = {
        type : "SELECT p_ftype.p_ftypeid as 'typeid', p_ftype.ftype as 'type', SUM(m_deal.amount) as 'count' FROM m_deal, productlist, p_type, p_ftype WHERE m_deal.productid = productlist.productid AND m_deal.returnornot = 0 AND productlist.p_typeid = p_type.p_typeid AND p_type.p_ftypeid = p_ftype.p_ftypeid GROUP BY p_ftype.ftype ORDER BY SUM(m_deal.amount) DESC LIMIT 3",
        other : "SELECT COUNT(m_deal.amount) as 'count' FROM m_deal WHERE m_deal.returnornot = 0"
    };
    async.map(sqls, function (item, callback) {
        db.query(item, function (err, results) {
            callback(err, results);
        });
    }, function (err, results) {
        if (err) {
            res.send(err);
        } else {
            var tdata = results;
            var threecount = 0;
            for (var i = 0 ; i < tdata.type.length;i++) {
                threecount += tdata.type[i].count;
            }
            var othercount = tdata.other[0].count;
            othercount -= threecount;
            var donutChartData = [
                {
                    color: "#F7464A",
                    highlight: "#FF5A5E",
                },
                {
                    color: "#46BFBD",
                    highlight: "#5AD3D1",
                },
                {
                    color: "#FDB45C",
                    highlight: "#FFC870",
                },
                {
                    color: "#a9a9a9",
                    highlight: "#c0c0c0",
                }
            ];
            if (tdata.type[0]) {
                donutChartData[0].value = tdata.type[0].count;
                donutChartData[0].label = tdata.type[0].type;
            }
            else {
                donutChartData = [
                    {
                        value: 0,
                        color: "#a9a9a9",
                        highlight: "#c0c0c0",
                        label: "無"
                    }
                ];
            }
            if (tdata.type[1]) {
                donutChartData[1].value = tdata.type[1].count;
                donutChartData[1].label = tdata.type[1].type;
            }
            else {
                donutChartData[1].value = 0;
            }
            if (tdata.type[2]) {
                donutChartData[2].value = tdata.type[2].count;
                donutChartData[2].label = tdata.type[2].type;
            }
            else {
                donutChartData[2].value = 0;
            }
            if (othercount >= 0) {
                donutChartData[3].value = othercount;
                donutChartData[3].label = "其他";
            }
            else {
                donutChartData[3].value = 0;
            }

            res.json(donutChartData);
            res.end();
        }
    });
});

router.get('/saletopproduct', function (req, res) {
    var db = req.db;
    db.query("SELECT SUM(m_deal.amount) as 'count', m_deal.productid as 'p_id' ,p_data.name as 'name' , productlist.click as 'click' FROM m_deal, productlist,p_data WHERE m_deal.productid = productlist.productid AND productlist.p_dataid = p_data.p_dataid AND m_deal.returnornot = 0 ORDER BY SUM(m_deal.amount) DESC LIMIT 3"
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                res.json(rows);
                res.end();
            }
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.get('/branchlocation', function (req, res) {
    var db = req.db;
    db.query("SELECT companylist.companyid as 'c_id', companylist.name as 'c_name',c_data.zipcode as 'zipcode' , c_data.district as 'district' , c_data.county as 'county', c_data.address as 'address' FROM companylist, c_data,c_account WHERE companylist.c_dataid = c_data.c_dataid AND companylist.c_account = c_account.c_account AND c_account.statue = 0;"
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                res.json(rows);
                res.end();
            }
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/location', function (req, res) {
    var db = req.db;
    var data = req.body;
    googleMapsClient.geocode({
        address: data.address
    }, function (err, response) {
        if (!err) {
            if (response.json.results[0]) {
                var location = response.json.results[0].geometry.location;
                location.i = data.i;
                location.c_name = data.c_name;
                res.json(location);
                res.end();
            }
        }
    });
});

router.post('/loadviews', function (req, res) {
    var db = req.db;
    var data = req.body;
    db.query("SELECT COUNT(views.times) as 'num' FROM views WHERE views.clicktime LIKE '"+data.month+"%'"
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                var tdata = rows[0];
                if (tdata.num == null) { res.json({'month': data.month , 'num': 0 }); res.end(); }
                else { res.json({ 'month': data.month , 'num': tdata.num }); res.end(); }
            }
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.get('/BarChartConpany', function (req, res) {
    var db = req.db;
    db.query("SELECT companylist.companyid as 'c_id' , companylist.name as 'name' FROM companylist;"
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                res.json(rows);
                res.end();
            }
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.get('/BarChartType', function (req, res) {
    var db = req.db;
    db.query("SELECT p_type.p_typeid as 't_id' , p_type.type as 'name' FROM p_type;"
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                res.json(rows);
                res.end();
            }
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});

router.post('/BarChart', function (req, res) {
    var db = req.db;
    var data = req.body;
    var sql = '';
    if (data.c_id == '0' && data.t_id == '0') {
        sql = " FROM m_deal WHERE  m_deal.date LIKE '" + data.month + "%'";
    }
    else if (data.c_id == '0') {
        sql = " FROM m_deal, productlist WHERE m_deal.productid = productlist.productid AND productlist.p_typeid = '" + data.t_id + "' AND m_deal.date LIKE '" + data.month + "%';";
    }
    else if (data.t_id == '0') {
        sql = " FROM m_deal WHERE m_deal.companyid = '" + data.c_id + "' AND m_deal.date LIKE '" + data.month + "%';";
    }
    else { 
        sql = " FROM m_deal, productlist WHERE  m_deal.companyid = '" + data.c_id + "' AND m_deal.productid = productlist.productid AND productlist.p_typeid = '" + data.t_id + "' AND m_deal.date LIKE '" + data.month + "%';";
    }
    db.query("SELECT SUM(m_deal.amount) as 'num' , SUM(m_deal.totalprice) as 'total'"+sql
, function (err, rows, fields) {
        if (!err) {
            if (rows[0]) {
                var tdata = rows[0];
                if (tdata.num == null && tdata.total != null) { res.json({ 'month': data.month ,'total' : tdata.total, 'num': 0 }); res.end(); }
                else if (tdata.num != null && tdata.total == null) { res.json({ 'month': data.month , 'total' : 0, 'num': tdata.num }); res.end(); }
                else if (tdata.total == null && tdata.num == null) { res.json({ 'month': data.month , 'total' : 0, 'num': 0 }); res.end(); }
                else { res.json({ 'month': data.month , 'total' : tdata.total, 'num': tdata.num }); res.end(); }
            }
        }
        else {
            console.log('Error while performing Query.');
        }
    });
});
module.exports = router;
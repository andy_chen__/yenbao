﻿var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var mysql = require('mysql');
//var db = mysql.createConnection({
//    host: '114.35.75.242',
//    port: '3307',
//    user: 'yenbao',
//    password: 'yenbao',
//    database: 'yenbao'
//});
//db.connect(function (err) {
//    if (!err) {
//        console.log("success get database ... nn");
//    }
//    else {
//        console.log("Error connecting database ... nn" + err.stack);
//    }
//});
var pool = mysql.createPool({
    //host: '114.35.75.242',
    host: 'localhost',
    port: '3306',
    user: 'root',
    password: 'x06m06cj/6',
    database: 'yenbao',
    // 無可用連線時是否等待pool連線釋放(預設為true)
    waitForConnections : true,
    // 連線池可建立的總連線數上限(預設最多為10個連線數)
    connectionLimit : 10
});

// 取得連線池的連線
pool.getConnection(function (err, connection) {
    
    if (err) {
        // 取得可用連線出錯
        console.log("Error connecting database ... nn" + err.stack);
    }
    else {
        // 成功取得可用連線
        
        console.log("success get database ... nn");
        // 使用取得的連線
        connection.query('SELECT something FROM sometable', function (err, rows) {
            // 使用連線查詢完資料
            
            // 釋放連線
            connection.release();
            // 不要再使用釋放過後的連線了，這個連線會被放到連線池中，供下一個使用者使用
        });
    }
});

var routes = require('./routes/index');
var backend = require('./routes/backend'); 
var branchManagementapi = require('./routes/branchManagementapi');//分店管理api
var memberManagementapi = require('./routes/memberManagementapi');//會員管理api
var carouselmanagementapi = require('./routes/carouselmanagementapi');//輪播管理api
var newsManagementapi = require('./routes/newsManagementapi');//最新消息管理api
var activityManagementapi = require('./routes/activityManagementapi');//活動消息管理api
var productManagementapi = require('./routes/productManagementapi');//商品管理api
var knowledgeManagementapi = require('./routes/knowledgeManagementapi');//知識分享管理api
var messageManagementapi = require('./routes/messageManagementapi');//連路我們
var ordersManagementapi = require('./routes/ordersManagementapi');//訂單
var companyProfileapi = require('./routes/companyProfileapi');//各黨api
var analysisManagementapi = require('./routes/analysisManagementapi');//流量分析api
//前台
var frontendapi = require('./routes/frontendapi');//前台api
//進銷存
var login = require('./routes/login');//登入api
var purchase = require('./routes/purchase');//進貨api
var sales = require('./routes/sales');//銷貨api 
var BaoZhengKaLeiYin = require('./routes/BaoZhengKaLeiYin');//保證卡列印api

var app = express();

// view engine setup
app.set('views', path.join(__dirname, './views'));
app.set('view engine', 'ejs');


app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(require('stylus').middleware(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'public')));

app.use(function (req, res, next) {
    req.db = pool;
    next();
});

app.use('/', routes);
app.use('/backend', backend);
app.use('/branchManagementapi', branchManagementapi);
app.use('/memberManagementapi', memberManagementapi);
app.use('/carouselmanagementapi', carouselmanagementapi); 
app.use('/newsManagementapi', newsManagementapi);
app.use('/activityManagementapi', activityManagementapi);
app.use('/productManagementapi', productManagementapi);
app.use('/knowledgeManagementapi', knowledgeManagementapi);
app.use('/messageManagementapi', messageManagementapi);
app.use('/ordersManagementapi', ordersManagementapi);
app.use('/companyProfileapi', companyProfileapi);
app.use('/analysisManagementapi' , analysisManagementapi);
//前台api
app.use('/frontendapi', frontendapi);
//進銷存
app.use('/login', login);
app.use('/purchase', purchase);
app.use('/sales', sales);
app.use('/BaoZhengKaLeiYin', BaoZhengKaLeiYin);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
        message: err.message,
        error: {}
    });
});


app.listen(5488);